# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Puttichai Lertkultanon <L.Puttichai@gmail.com>
#
# This file is part of pymanip.
#
# pymanip is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# pymanip is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# pymanip. If not, see <http://www.gnu.org/licenses/>.
import openravepy as orpy
import numpy as np
import pickle
from scipy.spatial import ConvexHull
import logging
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)
from .grasp import *
from . import utils
from .utils import Colorize
from .gripperinfo import Robotiq2F85Info

X = np.array([1., 0., 0.])
Y = np.array([0., 1., 0.])
Z = np.array([0., 0., 1.])

class BaseInfo(object):
    """This is the base class for object information for primitive geometrical shapes (box,
    cylinder, etc.). It has some common utilities implemented such as environment
    management.
    
    """
    def __init__(self, mobj, linkIndex, transformationSet, gripperType='2F85'):
        """
        Parameters
        ----------
        mobj : openravepy.KinBody
            Moveable object of interest
        linkIndex : int
            Index of the link to be considered
        transformationSet : list
            List of 4x4 transformation matrices of object in different placement classes
        gripperType : string
            Type of the gripper to generate grasp database with
        
        """
        self.objectName = mobj.GetName()
        self.objectID = mobj.GetKinematicsGeometryHash()
        self.linkIndex = linkIndex
        self.gripperType = gripperType
        # Relative transformation: from body's frame to link's frame
        self.Trel = np.dot(np.linalg.inv(mobj.GetTransform()),
                                         mobj.GetLinks()[linkIndex].GetTransform())

        self._moveInOffset = dict()
        self._moveInOffset[0] = 0.004
        self._moveInOffset[1] = 0.004
        self._moveInOffset[2] = 0.004
        self._moveInOffset[3] = 0.004
        self._moveInOffset[4] = 0.004
        self._moveInOffset[5] = 0.004

        # Create its own copy of OpenRAVE environment for processing
        self._CloneEnvironment(mobj)
        self._Preprocess(transformationSet)
        self._DestroyEnvironment()


    def _CloneEnvironment(self, mobj):
        """Create a private copy of an OpenRAVE environment.

        """
        self._env = orpy.Environment()
        self._env.Clone(mobj.GetEnv(), 1) # cloningOption = 1
        checkerName = mobj.GetEnv().GetCollisionChecker().GetXMLId()
        self._collisionChecker = orpy.RaveCreateCollisionChecker(self._env, checkerName)
        self._env.SetCollisionChecker(self._collisionChecker)

        # Remove unrelated bodies from the environment
        for body in self._env.GetBodies():
            if not (body.GetName() == self.objectName):
                self._env.Remove(body)
        assert(len(self._env.GetBodies()) == 1)
        self._object = self._env.GetKinBody(self.objectName)
        self._link = self._object.GetLinks()[self.linkIndex]

        # Create a floor for testing
        floor = orpy.RaveCreateKinBody(self._env, '')
        floor.InitFromBoxes(np.array([[0.0, 0.0, -0.050001, 10.0, 10.0, 0.05]]))
        floor.SetName('floor')
        self._env.Add(floor)

        # Load a gripper
        if self.gripperType == '2F85':
            self.gripperInfo = Robotiq2F85Info()
            self._gripper = self._env.ReadKinBodyXMLData(self.gripperInfo._XMLData)
            self._env.Add(self._gripper)
        else:
            raise ValueError, "Unknown gripper"


    def _DestroyEnvironment(self):
        """Destroy all openrave related object.

        """
        del self._link
        del self._object
        del self._gripper
        del self._collisionChecker
        self._env.Destroy()
        del self._env


    def ComputeTGripper(self, mobj, qgrasp):
        raise NotImplementedError


    def _Preprocess(self, transformationSet):
        raise NotImplementedError


class BoxInfo(BaseInfo):
    """Store grasp information for an OpenRAVE box.

    """
    def __init__(self, mobj, linkIndex, transformationSet, gripperType='2F85'):
        # Check soundness
        assert(mobj.GetLinks()[linkIndex].GetGeometries()[0].GetType() == orpy.GeometryType.Box)
        super(BoxInfo, self).__init__(mobj, linkIndex, transformationSet, gripperType)

    
    @property
    def approachingDirections(self):
        """approachingDirections is a dictionary. Each key is a placement class index. The 
        corresponding value is a list of all possible approaching direction when the object is placed
        at a (nominal) transformation from that placement class.
        """
        return self._approachingDirections

    @property
    def slidingDirections(self):
        """slidingDirections is a dictionary. Each key is a valid approaching direction. The
        corresponding value is a list of valid sliding direction given that approaching direction.
        """
        return self._slidingDirections

    @property
    def intervals(self):
        """intervals is a dictionary. For a box, a key of intervals is a tuple (isurface,
        approachingDirection, slidingDirection). The corresponding value is a list containing
        intervals of valid sliding range for that placement class + approachginDir + slidingDir:

                  intervals = [interval_0, interval_1, ..., interval_n],

        where interval_i = [delta_start, interval_length]
        """
        return self._intervals

    @property
    def moveInOffset(self):
        """moveInOffset is a dictionary. Keys are approaching directions (6 in total). Values are
        offsets for each approaching direction.

        When the gripper is aligned with the specified approaching and sliding directions,
        at first the gripper will be positioned such that its local frame's origin is
        right at the edge of the object. However, if it grasps the object right away, the
        grasp might not be firm enough due to small contact region so the gripper needs to
        move *towards* the object a bit more. This moveInOffset specifies how much the
        gripper needs to move towards the object.

        """
        return self._moveInOffset


    def ComputeTGripper(self, mobj, qgrasp, standOff=0):
        """Given a grasp parameter qgrasp of the form
        
                  qgrasp = [linkIndex, approachingDir, sligingDir, delta],

        compute the corresponding gripper transformation. Note that the approachingDirection may be
        given as the real approaching direction (an integer between 0 to 5 inclusive) or in the form
        
                  approachingDir = 6*linkIndex + realApproachinDir.

        standOff (non-negative float) is the distance for the gripper to move in the
        direction of *negative* approaching direction. The idea is for standOff > 0, the
        calculated gripper transformation will be a *pregrasp* configuration instead of a
        grasping configuration.

        """
        linkIndex, approachingDir, slidingDir, delta = qgrasp
        assert(linkIndex == self.linkIndex)

        if standOff < 0:
            standOff = -standOff

        realApproachingDir = np.mod(approachingDir, 6)

        # Now align the gripper according to the approaching direction and sliding direction
        if (realApproachingDir == pX):
            T0 = utils.ComputeTRot(pY, 0.5*np.pi)
            if (slidingDir == pZ):
                T1 = utils.ComputeTRot(pZ, np.pi)
            elif (slidingDir == pY):
                T1 = utils.ComputeTRot(pZ, 0.5*np.pi)
            elif (slidingDir == mZ):
                T1 = np.eye(4)
            elif (slidingDir == mY):
                T1 = utils.ComputeTRot(pZ, -0.5*np.pi)
        elif (realApproachingDir == mX):
            T0 = utils.ComputeTRot(pY, -0.5*np.pi)
            if (slidingDir == pZ):
                T1 = np.eye(4)
            elif (slidingDir == pY):
                T1 = utils.ComputeTRot(pZ, -0.5*np.pi)
            elif (slidingDir == mZ):
                T1 = utils.ComputeTRot(pZ, np.pi)
            elif (slidingDir == mY):
                T1 = utils.ComputeTRot(pZ, 0.5*np.pi)
        elif (realApproachingDir == pY):
            T0 = utils.ComputeTRot(pX, -0.5*np.pi)
            if (slidingDir == pX):
                T1 = np.eye(4)
            elif (slidingDir == pZ):
                T1 = utils.ComputeTRot(pZ, -0.5*np.pi)
            elif (slidingDir == mX):
                T1 = utils.ComputeTRot(pZ, np.pi)
            elif (slidingDir == mZ):
                T1 = utils.ComputeTRot(pZ, 0.5*np.pi)
        elif (realApproachingDir == mY):
            T0 = utils.ComputeTRot(pX, 0.5*np.pi)
            if (slidingDir == pX):
                T1 = np.eye(4)
            elif (slidingDir == pZ):
                T1 = utils.ComputeTRot(pZ, 0.5*np.pi)
            elif (slidingDir == mX):
                T1 = utils.ComputeTRot(pZ, np.pi)
            elif (slidindDir == mZ):
                T1 = utils.ComputeTRot(pZ, -0.5*np.pi)
        elif (realApproachingDir == pZ):
            T0 = np.eye(4)
            if (slidingDir == pX):
                T1 = np.eye(4)
            elif (slidingDir == pY):
                T1 = utils.ComputeTRot(pZ, 0.5*np.pi)
            elif (slidingDir == mX):
                T1 = utils.ComputeTRot(pZ, np.pi)
            elif (slidingDir == mY):
                T1 = utils.ComputeTRot(pZ, -0.5*np.pi)
        else:
            T0 = utils.ComputeTRot(pX, np.pi)
            if (slidingDir == pX):
                T1 = np.eye(4)
            elif (slidingDir == pY):
                T1 = utils.ComputeTRot(pZ, -0.5*np.pi)
            elif (slidingDir == mX):
                T1 = utils.ComputeTRot(pZ, np.pi)
            elif (slidingDir == mY):
                T1 = utils.ComputeTRot(pZ, 0.5*np.pi)
                
        d = self.extents[slidingDir]
        zoffset = (-self.extents[np.mod(realApproachingDir, 3)] +
                   self._moveInOffset[realApproachingDir] - standOff)
        T2 = utils.ComputeTTrans(pZ, zoffset)
        T3 = utils.ComputeTTrans(pX, delta)
        # Tlink = mobj.GetLinks()[self.linkIndex].GetTransform()
        Tlink = np.dot(mobj.GetTransform(), self.Trel)
        return reduce(np.dot, [Tlink, T0, T1, T2, T3])


    def _Preprocess(self, transformationSet):
        """For each placement (given in transformationSet), examine corresponding valid approaching
        direction. For each valid approaching direction, examine possible sliding direction. For
        each sliding direction, examine possible sliding range.

        Requirement: the box's local frame must coincide with the center of the box.

        Note: 
        
        1. All directions are expressed in the box's local frame.

        2. For boxes, a grasp parameter qgrasp can be expressed as
        
                  qgrasp = [..., approachingDir, slidingDir, delta]
        """
        self.extents = self._object.GetLinks()[self.linkIndex].GetGeometries()[0].GetBoxExtents()
        
        self._GetPossibleSlidingDirections()
        self._approachingDirections = dict()
        self._intervals = dict()

        with self._object:
            for (isurface, T) in transformationSet:
                self._object.SetTransform(T)
                plink = self._link.GetGlobalCOM()
                Tlink = self._link.GetTransform()

                xVect = np.reshape(np.array(Tlink[0:3, pX]), (3, ))
                yVect = np.reshape(np.array(Tlink[0:3, pY]), (3, ))
                zVect = np.reshape(np.array(Tlink[0:3, pZ]), (3, ))

                self.approachingDirections[isurface] = []
                # Now check each of six surfaces of the box if it can be approached by the gripper
                for direction in [pX, pY, pZ, mX, mY, mZ]:
                    approachingVect = np.reshape(np.array(Tlink[0:3, np.mod(direction, 3)]), (3, ))
                    if direction > pZ:
                        approachingVect *= -1.0

                    aZ = np.dot(approachingVect, Z)

                    if aZ > 0:
                        # The gripper is pointing upward.
                        neededOffset = aZ*(self.extents[np.mod(direction, 3)] + self.gripperInfo.L)
                        # Note that aZ is actually cosine of the angle between the gripper axis and
                        # the world's +Z axis.

                        if neededOffset > self.gripperInfo.L:
                            # The link is too low (= too close to the floor). Grasping from this
                            # approaching direction will definitely cause collisions.
                            continue
                        else:
                            # There might be no problem here
                            pass
                    elif np.allclose(aZ, 0):
                        # The gripper is approaching the link from sideway.
                        if plink[2] < self.gripperInfo.gripperOffset:
                            # The link is too low (= too close to the floor). Grasping from this
                            # approaching direction will definitely cause collisions.
                            continue
                        else:
                            # There might be no problem here
                            pass
                    else:
                        # There might be no problem here
                        pass

                    # If arrive here, this approaching direction is a candidate.
                    possibleSlidingDirections = self.slidingDirections[direction]
                    validApproachingDir = False

                    if len(possibleSlidingDirections) == 2:
                        slidingDir0 = possibleSlidingDirections[0]
                        interval0 = self._GetSlidingRanges(direction, slidingDir0, case=2)
                        if len(interval0) > 0:
                            self.intervals[isurface, direction, slidingDir0] = interval0
                            validApproachingDir = True

                        slidingDir1 = possibleSlidingDirections[1]
                        interval1 = self._GetSlidingRanges(direction, slidingDir1, case=2)
                        if len(interval1) > 0:
                            self.intervals[isurface, direction, slidingDir1] = interval1
                            validApproachingDir = True

                    elif len(possibleSlidingDirections) == 1:
                        slidingDir = possibleSlidingDirections[0]
                        interval = self._GetSlidingRanges(direction, slidingDir)
                        if len(interval) > 0:
                            self.intervals[isurface, direction, slidingDir] = interval
                            validApproachingDir = True

                    if validApproachingDir:
                        self.approachingDirections[isurface].append(direction)


    def _GetPossibleSlidingDirections(self):
        """Given the max distance that the gripper can open, dmax, examine possible sliding
        directions for each case of approaching direction.
        
        """
        dx, dy, dz = self.extents
        self._slidingDirections = dict()

        # Examine each approaching direction
        # Approaching direction is +X/-X
        self._slidingDirections[pX] = []
        if (dz < self.gripperInfo.dmax):
            self._slidingDirections[pX].append(pY)
        if (dy < self.gripperInfo.dmax):
            self._slidingDirections[pX].append(pZ)

        self._slidingDirections[mX] = self._slidingDirections[pX]

        # Approaching direction is +Y/-Y
        self._slidingDirections[pY] = []
        if (dz < self.gripperInfo.dmax):
            self._slidingDirections[pY].append(pX)
        if (dx < self.gripperInfo.dmax):
            self._slidingDirections[pY].append(pZ)

        self._slidingDirections[mY] = self._slidingDirections[pY]

        # Approaching direction is +Z/-Z
        self._slidingDirections[pZ] = []
        if (dy < self.gripperInfo.dmax):
            self._slidingDirections[pZ].append(pX)
        if (dx < self.gripperInfo.dmax):
            self._slidingDirections[pZ].append(pY)

        self._slidingDirections[mZ] = self._slidingDirections[pZ]


    def _GetSlidingRanges(self, approachingDir, slidingDir, case=1):
        """Examine the possible gripper sliding ranges. Assume that the object is already set in
        place.

        Each element in interval is of the form (start, length).
      
        """
        d = self.extents[np.mod(slidingDir, 3)]
        step = 0.04 # step along the direction with this resolution
        
        qgrasp = [self.linkIndex, approachingDir, slidingDir, 0.0]
        # Tgripper places the gripper at the middle of the object length
        Tgripper = self.ComputeTGripper(self._object, qgrasp)

        interval = []
        if case == 2:
            # In this case, there are two possible sliding directions with this
            # approaching direction. Therefore, we don't step anywhere.
            self._gripper.SetTransform(Tgripper)
            if not self._env.CheckCollision(self._gripper):
                interval = [(-0.25*step, 0.5*step)]

        else:
            domain = np.linspace(-d, d, int(2*d/step) + 1)
            Tstep = np.eye(4)
            Tstep[0, 3] = domain[0] # stepping along the sliding axis of the gripper

            self._gripper.SetTransform(Tgripper)
            inCollision = self._env.CheckCollision(self._gripper)
            prevStart = None
            if not inCollision:
                prevStart = domain[0]

            for i in xrange(1, len(domain)):
                Tstep[0, 3] = domain[i]
                T = np.dot(Tgripper, Tstep)
                self._gripper.SetTransform(T)
                inCollision = self._env.CheckCollision(self._gripper)

                if not inCollision:
                    if prevStart is not None:
                        # non-collision range continues
                        continue
                    else:
                        # This is the first step in a new non-collision range
                        prevStart = domain[i]
                else:
                    if prevStart is not None:
                        # non-collision range just ended
                        if np.allclose(domain[i - 1], prevStart):
                            # non-collision range has only one step.
                            interval.append((prevStart - 0.25*step, 0.5*step))
                        else:
                            interval.append((prevStart, domain[i - 1] - prevStart))
                        prevStart = None
                    else:
                        # in-collision range continues
                        continue

            if not inCollision:
                if prevStart is not None:
                    # non-collision range reaches the end
                    interval.append((prevStart, domain[-1] - prevStart))
                else:
                    # a new non-collision range starts right at the end
                    interval.append((prevStart - 0.25*step, 0.25*step))
            else:
                pass

        return np.around(interval, decimals=6).tolist()
