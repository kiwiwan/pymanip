import openravepy as orpy
import numpy as np
import scipy as sp
from scipy.spatial import ConvexHull
from copy import deepcopy
import time
import trimesh
import logging
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)

# CVXOPT
import cvxopt
import cvxopt.solvers
from cvxopt import matrix as cvxmat
from warnings import warn
try:
    import cvxopt.glpk
    __default_solver = 'glpk'
    # GLPK is the fastest LP solver I could find so far:
    # <https://scaron.info/blog/linear-programming-in-python-with-cvxopt.html>
    # ... however, it's verbose by default, so tell it to STFU:
    cvxopt.solvers.options['glpk'] = {'msg_lev': 'GLP_MSG_OFF'}  # cvxopt 1.1.8
    cvxopt.solvers.options['msg_lev'] = 'GLP_MSG_OFF'  # cvxopt 1.1.7
    cvxopt.solvers.options['LPX_K_MSGLEV'] = 0  # previous versions
except ImportError:
    __default_solver = None
cvxopt.solvers.options['show_progress'] = False  # disable cvxopt output

from . import utils

_qhull_prec1 = 'E0.001'
_qhull_prec2 = 'E0.0001'


class ContactSet(object):
    """ContactSet keeps information of a set of contact points on the same contact surface.

    """
    def __init__(self, points, R, isLocal=True):
        """
        Parameters
        ----------
        points : list
            List of (3D) contact points
        R : numpy.ndarray
            Rotation matrix describing a frame attached to the contact surface.
        isLocal : bool
            If True, points and R are described w.r.t the COM of the manipulated object. 
            Otherwise, points and R are described in the global frame.
        
        """
        self.contacts = deepcopy(points)
        self.R = np.array(R)
        self.nContacts = len(self.contacts)
        self.isLocal = isLocal
        self._G = None
    

    def ComputeGraspMatrix(self, Tcom=None):
        """Compute a grasp matrix G which converts contact forces to a contact wrench, i.e., w = Gf.

        """
        if not self.isLocal:
            if self._G is None:
                G = []
                I = np.eye(3)
                for point in self.contacts:
                    G.append(np.vstack([I, utils.ComputeSkewFromVect(point)]))
                self._G = np.hstack(G)
            return self._G

        if Tcom is None:
            log.warn("This contact set is local. Tcom needs to be given")
            return False
        
        transformedPoints = [utils.Transform(point, Tcom) for point in self.contacts]
        G = []
        I = np.eye(3)
        for point in transformedPoints:
            G.append(np.vstack([I, utils.ComputeSkewFromVect(point)]))
        return np.hstack(G)

        
def GetContactSets(robot, mobj, qgrasp, TcomLocal, returnLocal=True,
                   fingertipnames={'left': 'robotiq_fingertip_l', 'right': 'robotiq_fingertip_r'}):
    """Assuming that 
    
    1. the robot is already grasping the object (fingertips touching the object).

    2. The gripper is a robotiq gripper.

    This function returns two contact sets, each one corresponds to a contact set on one finger tip
    surface.

    Parameters
    ----------
    robot : openrave.Robot
    mobj : openrave.KinBody
    qgrasp : list
        qgarsp = [graspedLinkIndex, approachingDir, slidingDir, delta]
    TcomLocal : 4x4 transformation matrix
        Transformation of the COM described in the object's local frame

    """

    lpoints, rpoints = GetRawContactPoints(robot, mobj, fingertipnames)
    lpoints, rpoints, Rl, Rr = ProcessRawContactPoints(lpoints, rpoints, robot, mobj, qgrasp, returnLocal, TcomLocal)
    lContactSet = ContactSet(lpoints, Rl, isLocal=returnLocal)
    rContactSet = ContactSet(rpoints, Rr, isLocal=returnLocal)
    return lContactSet, rContactSet


def GetRawContactPoints(robot, mobj, fingertipnames={'left': 'robotiq_fingertip_l', 'right': 'robotiq_fingertip_r'}):
    """Get raw contact points (described in the global frame) by using pqp collision checker.

    Returns
    -------
    lpoints : list
        Contact points on the left finger tip
    rpoints : list
        Contact points on the right finger tip
    
    """
    isGrabbing = robot.IsGrabbing(mobj) is not None
    if isGrabbing:
        # Need to release first, otherwise we cannot detect contacts.
        robot.Release(mobj)
        
    # Change the collision checker to pqp in order to be able to obtain contact points.
    lcolRep = orpy.CollisionReport()
    rcolRep = orpy.CollisionReport()
    env = robot.GetEnv()
    prevChecker = env.GetCollisionChecker()
    usingNewChecker = not (prevChecker.GetXMLId() == 'pqp')
    if usingNewChecker:
        env.SetCollisionChecker(orpy.RaveCreateCollisionChecker(env, 'pqp'))
    env.CheckCollision(robot.GetLink(fingertipnames['left']), mobj, report=lcolRep)
    env.CheckCollision(robot.GetLink(fingertipnames['right']), mobj, report=rcolRep)
    if usingNewChecker:
        env.SetCollisionChecker(prevChecker)
        
    # Extract all detected contact points
    lpoints = [c.pos for c in lcolRep.contacts]
    rpoints = [c.pos for c in rcolRep.contacts]
    if len(lpoints) == 0:
        import IPython; IPython.embed(header="No contact detected on the left fingertip")
        raise Exception("No contact detected on the left fingertip")
    if len(rpoints) == 0:
        import IPython; IPython.embed(header="No contact detected on the left fingertip")
        raise Exception("No contact detected on the right fingertip")
    
    # Restore the grabbing state
    if isGrabbing:
        robot.Grab(mobj)
    return lpoints, rpoints


def ProcessRawContactPoints(lpoints, rpoints, robot, mobj, qgrasp, computeLocal=False, TcomLocal=None):
    """For each set of contact points, first we compute the convex hull (2D) to filter out unnecessary
    contact points. (We need only the one at the vertices of the contact polygon.) Then compute
    rotation matrices of the frames attached to the contact surfaces.
    
    Parameters
    ----------
    lpoints : list
    rpoints : list
    mobj : openrave.KinBody
    qgrasp : list
    computeLocal : bool
        If True, the processed contact points and the rotation matrices will be expressed w.r.t. to 
        the frame attached to the object's COM.
    
    Returns
    -------
    processedlpoints : list
    processedrpoints : list
    Rl : rotation matrix
    Rr : rotation matrix
    
    """
    approachingDir, slidingDir, delta = qgrasp[-3:] # unpack data
    realApproachingDir = np.mod(approachingDir, 6)
    ibox = int(approachingDir/6)
    link = mobj.GetLinks()[ibox]
    extents = link.GetGeometries()[0].GetBoxExtents()
    # Find out what the grippingDir (the direction normal to a finger tip surface) is
    if np.mod(realApproachingDir, 3) == 0:
        # Approaching direction is +X/-X
        if np.mod(slidingDir, 3) == 1:
            extent = extents[2]
            grippingDir = 2
        elif np.mod(slidingDir, 3) == 2:
            extent = extents[1]
            grippingDir = 1
    elif np.mod(realApproachingDir, 3) == 1:
        # Approaching direction is +Y/-Y
        if np.mod(slidingDir, 3) == 0:
            extent = extents[2]
            grippingDir = 2
        elif np.mod(slidingDir, 3) == 2:
            extent = extents[0]
            grippingDir = 0
    elif np.mod(realApproachingDir, 3) == 2:
        # Approaching direction is +Y/-Y
        if np.mod(slidingDir, 3) == 0:
            extent = extents[1]
            grippingDir = 1
        elif np.mod(slidingDir, 3) == 1:
            extent = extents[0]
            grippingDir = 0

    # Create a convex hull of all contact points (for removing redundant points). We need only the
    # contact points that are vertices of the contact surface.

    # We first transform all points into the link's frame because in the link's frame, there will be
    # one coordinate which is (approximately) constant.
    Tlink = link.GetTransform()
    TlinkInv = np.linalg.inv(Tlink)
    lLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in lpoints]
    rLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in rpoints]

    # All the points (in the link's local frame) are coplanar. The plane is actuall described by
    # grippingDir. Due to some numerical error, we compute the average of that *constant*
    # coordinate.    
    lAvgCoord = np.average([p[grippingDir] for p in lLinkLocalPoints])
    rAvgCoord = np.average([p[grippingDir] for p in rLinkLocalPoints])

    # Process the left contact surface
    lLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in lLinkLocalPoints]
    # QHULL OPTIONS: (Comments written by Stephane Caron)
    #
    # - ``Pp`` -- do not report precision problems
    # - ``Q0`` -- no merging with C-0 and Qx
    #
    # ``Q0`` avoids [this bug](https://github.com/scipy/scipy/issues/6484).  It slightly
    # diminishes computation times (0.9 -> 0.8 ms on my machine) but raises QhullError at
    # the first sight of precision errors.
    #
    # https://github.com/stephane-caron/3d-com-mpc/commit/27c13b4030ad8af30ceba2997b9a4ac657c07215
    try:
        lHull = ConvexHull(lLinkLocalPoints2d, qhull_options="Pp Q0 E0.001")
    except:
        try:
            lHull = ConvexHull(lLinkLocalPoints2d, qhull_options="Pp Q0")
        except:
            import IPython; IPython.embed(header='ConvexHull error (L)')
    lHullLocalPoints2d = [lHull.points[i].tolist() for i in lHull.vertices] # a set of non-redundant points
    lHullLocalPoints = deepcopy(lHullLocalPoints2d)
    for point in lHullLocalPoints:
        point.insert(grippingDir, lAvgCoord)
    lHullLocalPoints = [np.asarray(p) for p in lHullLocalPoints]
    
    # Process the right contact surface
    rLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in rLinkLocalPoints]
    try:
        rHull = ConvexHull(rLinkLocalPoints2d, qhull_options="Pp Q0 E0.001")
    except:
        try:
            rHull = ConvexHull(rLinkLocalPoints2d, qhull_options="Pp Q0")
        except:
            import IPython; IPython.embed(header='ConvexHull error (R)')
    rHullLocalPoints2d = [rHull.points[i].tolist() for i in rHull.vertices] # a set of non-redundant points
    rHullLocalPoints = deepcopy(rHullLocalPoints2d)
    for point in rHullLocalPoints:
        point.insert(grippingDir, rAvgCoord)
    rHullLocalPoints = [np.asarray(p) for p in rHullLocalPoints]

    # Now transform all filtered points back into the global frame
    processedlpoints = [utils.Transform(p, Tlink) for p in lHullLocalPoints]
    processedrpoints = [utils.Transform(p, Tlink) for p in rHullLocalPoints]

    # Now we compute the description of the contact surface's frame (= obtain normal and tangential
    # components). We make use of the information of the gripper's transformation.
    Tmanip = robot.GetActiveManipulator().GetTransform()
    Rmanip = utils.ExtractRotationMatrix(Tmanip)

    # Rl is the rotation matrix describing the contact surface's frame of the surface touchuing the
    # gripper's left finger tip. The last column represent the normal vector of the surface's frame.
    # Since we are considering the free body diagram of the object, the normal vector should be
    # pointing *inward* (toward the object).

    # The following calculations are based on the current gripper configuration (denso + robotiq
    # gripper). If the robot changes, we also need to modify this.
    Rl = np.eye(3)
    Rl[0:3, 0] = Rmanip[0:3, 0]
    Rl[0:3, 1] = -Rmanip[0:3, 2]
    Rl[0:3, 2] = Rmanip[0:3, 1]
    Rr = np.eye(3)
    Rr[0:3, 0] = -Rmanip[0:3, 0]
    Rr[0:3, 1] = -Rmanip[0:3, 2]
    Rr[0:3, 2] = -Rmanip[0:3, 1]

    if not computeLocal:
        return processedlpoints, processedrpoints, Rl, Rr

    # Here we need to return local descriptions of everything we have. Important note: the local
    # description is with respect to the frame attached to the COM (which might not coincide with
    # the object's local frame!!!)
    if TcomLocal is not None:
        Tcom = np.dot(mobj.GetTransform(), TcomLocal)
    else:
        Tcom = mobj.GetTransform()
    TcomInv = np.linalg.inv(Tcom)

    lLocalPoints = [utils.Transform(p, TcomInv) for p in processedlpoints]
    rLocalPoints = [utils.Transform(p, TcomInv) for p in processedrpoints]
    RlLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), Rl)
    RrLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), Rr)
    return lLocalPoints, rLocalPoints, RlLocal, RrLocal
    

##########################################################################################

def ComputeGraspConstraintCoeffs(contactset, mu, fmax=None, Tcom=None):
    """
    """
    if Tcom is not None:
        R = np.dot(utils.ExtractRotationMatrix(Tcom), contactset.R)
    else:
        R = contactset.R
    normal = R[0:3, 2]
    t1 = R[0:3, 0]
    t2 = R[0:3, 1]
    Upoint = np.vstack([-normal,
                        -t1 - mu*normal,
                         t1 - mu*normal,
                        -t2 - mu*normal,
                         t2 - mu*normal])
    U = [Upoint] * contactset.nContacts
    Usurf = sp.linalg.block_diag(*U)
    b = np.zeros(5 * contactset.nContacts)

    if fmax is not None:
        # Normal components of all contact forces must sum up to no greater than fmax.
        UlastRow = np.tile(normal, contactset.nContacts)
        Usurf = np.vstack([Usurf, UlastRow])
        b = np.append(b, fmax)

    return Usurf, b

    
def CheckGraspEquilibrium(robots, mobj, qgrasps, Tobj, myObject, fmax, mu):
    """Check if the current grasping configurations is in static equilibrium.

    """
    TcomLocal = myObject.Tcom
    Tcom = np.dot(Tobj, TcomLocal)
    pcom = Tcom[0:3, 3]

    # Calculate the object's mass
    env = robots[0].GetEnv()
    g = env.GetPhysicsEngine().GetGravity()
    m = sum([link.GetMass() for link in mobj.GetLinks()])

    # Gravito-inertial wrench is such that wgi + Gf = 0, where G is a grasp matrix and f is a
    # stacked contact forces vector.
    wgi = np.hstack([m*g, m*np.cross(pcom, g)])

    U_all = []
    b_all = []
    G_all = []
    for (irobot, (robot, qgrasp)) in enumerate(zip(robots, qgrasps)):
        lContactSet, rContactSet = GetContactSets(robot, mobj, qgrasp, TcomLocal, returnLocal=False)
        G_l = lContactSet.ComputeGraspMatrix()
        G_r = rContactSet.ComputeGraspMatrix()
        Usurf_l, b_l = ComputeGraspConstraintCoeffs(lContactSet, mu, fmax)
        Usurf_r, b_r = ComputeGraspConstraintCoeffs(rContactSet, mu, fmax)

        G_all.append(G_l)
        G_all.append(G_r)
        U_all.append(Usurf_l)
        U_all.append(Usurf_r)
        b_all.append(b_l)
        b_all.append(b_r)

    """
    Now solve the following LP:

    minimize(f)  c*f := 0 (feasibility problem)
    subject to   Uf <= b
                 Gf + w = 0
    """
    G = np.hstack(G_all)
    U = sp.linalg.block_diag(*U_all)
    b = np.hstack(b_all)
    c = np.zeros(U.shape[1])
    sol = cvxopt.solvers.lp(cvxmat(c), cvxmat(U), cvxmat(b), cvxmat(G), cvxmat(-wgi))
    passed = 'optimal' in sol['status']
    return passed
    

def CheckGraspEquilibriumAlongTrajectory(cctraj, robots, mobj, qgrasps, myObject, fmax, mu, taskmanips=None):
    """
    """
    se3Traj = cctraj.se3_traj
    bwaypoints = cctraj.bimanual_wpts
    timestamps = cctraj.timestamps

    def OpenGrippers(robots):
        for robot in robots:
            robot.SetDOFValues([0], [6])

    # Prepare grasping configurations
    lcontroller = robots[0].GetController()
    rcontroller=  robots[1].GetController()
    if taskmanips is None:
        taskmanips = []
        for robot in robots:
            taskmanips.append(orpy.interfaces.TaskManipulation(robot))
    TcomLocalInv = np.linalg.inv(myObject.Tcom)
    env = mobj.GetEnv()
    OpenGrippers(robots)
    for (irobot, robot) in enumerate(robots):
        robot.SetActiveDOFValues(bwaypoints[irobot][0])
    mobj.SetTransform(np.dot(se3Traj.Eval(timestamps[0]), TcomLocalInv))

    # Here we need to enable only boxes. Mesh + boxes can be a bit misaligned (of course,
    # boxes are created almost manually) and this may cause one fingertip to be collision
    # before another.
    linkEnabled = []
    for l in mobj.GetLinks():
        if l.IsEnabled():
            linkEnabled.append(1)
        else:
            linkEnabled.append(0)
        
        if l.GetGeometries()[0].GetType() == orpy.GeometryType.Box:
            l.Enable(True)
        elif l.GetGeometries()[0].GetType() == orpy.GeometryType.Trimesh:
            l.Enable(False)        

    for taskmanip in taskmanips:
        taskmanip.CloseFingers()
    while not lcontroller.IsDone() or not rcontroller.IsDone():
        time.sleep(0.001)

    # Get local contact sets
    ts_contactset = time.time()
    TcomLocal = myObject.Tcom
    localContactSet = dict()
    for (irobot, robot) in enumerate(robots):
        localContactSet[irobot] = GetContactSets(robot, mobj, qgrasps[irobot], TcomLocal, returnLocal=True)
    te_contactset = time.time()
    print "Generated 2 contact sets in {0} sec.".format(te_contactset - ts_contactset)

    # Restore the previous states of links after finish contact points processing
    for wasEnabled, l in zip(linkEnabled, mobj.GetLinks()):
        if wasEnabled:
            l.Enable(True)
        else:
            l.Enable(False)

    # Get if the contact sets can generate feasible contact forces at every time instant
    g = env.GetPhysicsEngine().GetGravity()
    m = sum([link.GetMass() for link in mobj.GetLinks()])    

    t0 = time.time()
    for (itimestamp, t) in enumerate(timestamps):
        Tcom = se3Traj.Eval(t) # the verifier plans COM traj directly
        
        wgi = np.hstack([m*g, m*np.cross(Tcom[0:3, 3], g)])
        U_all = []
        b_all = []
        G_all = []
        for (irobot, (lcontactset, rcontactset)) in localContactSet.iteritems():
            G_l = lcontactset.ComputeGraspMatrix(Tcom)
            G_r = rcontactset.ComputeGraspMatrix(Tcom)
            Usurf_l, b_l = ComputeGraspConstraintCoeffs(lcontactset, mu, fmax, Tcom)
            Usurf_r, b_r = ComputeGraspConstraintCoeffs(rcontactset, mu, fmax, Tcom)
            G_all.append(G_l)
            G_all.append(G_r)
            U_all.append(Usurf_l)
            U_all.append(Usurf_r)
            b_all.append(b_l)
            b_all.append(b_r)
        """
        Now solve the following LP:
        
        minimize(f)  c*f := 0 (feasibility problem)
        subject to   Uf <= b
                     Gf + w = 0
        """
        G = np.hstack(G_all)
        U = sp.linalg.block_diag(*U_all)
        b = np.hstack(b_all)
        c = np.zeros(U.shape[1])
        sol = cvxopt.solvers.lp(cvxmat(c), cvxmat(U), cvxmat(b), cvxmat(G), cvxmat(-wgi))
        passed = 'optimal' in sol['status']
        if not passed:
            log.info("Grasp equilibrium violated at t = {0}; itimstamp = {1}".format(t, itimestamp))
            qrobot_l = np.array(bwaypoints[0][itimestamp])
            qrobot_r = np.array(bwaypoints[1][itimestamp])
            log.info("Info:\nTcom = np.{0}\nqrobot_l = np.{1}\nqrobot_r = np.{2}".format\
                     (repr(Tcom), repr(qrobot_l), repr(qrobot_r)))
            OpenGrippers(robots)
            return False
    t1 = time.time()
    print "Checking {0} points in {1} sec. = {2} sec/point".format(len(timestamps), t1 - t0, (t1 - t0)/len(timestamps))
    OpenGrippers(robots)
    return True
