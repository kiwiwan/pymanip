# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 Puttichai Lertkultanon <L.Puttichai@gmail.com>
#
# This file is part of pymanip.
#
# pymanip is free software: you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# pymanip is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# pymanip. If not, see <http://www.gnu.org/licenses/>.
import openravepy as orpy
import numpy as np
import pickle
import networkx as nx
from scipy.spatial import ConvexHull
import logging
import time
from itertools import product
import trimesh as tm
tm.constants.tol.facet_rsq = 1e7
tm.constants.log.setLevel(logging.INFO)
logging.basicConfig(format='[%(levelname)s] [%(name)s: %(funcName)s] %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)
import random
_rng = random.SystemRandom()
from .grasp import *
from . import utils
from .utils import Colorize

# from .objectinfo import (BoxInfo, CylinderInfo)
from .objectinfo import BoxInfo
import os
_pymanipdir = os.path.join(os.path.expanduser('~'), '.pymanip')

import pkg_resources
class MyObject(object):
    """This class preprocess and store all necessary information about the given object. This
    information will be used in the planning.

    1. Create a mesh of the object's convex hull and compute related information such as
    placements, face adjacency graph.

    2. Input movable object can be either 
        2.1 composed entirely of boxes (i.e., created using OpenRAVE's boxes), or
        2.2 mesh with boxes

    For 2.2, we assume that this is the processed model and there exists a file of the same
    name in off format which contains the original mesh model (e.g., see models/ikea-stefan.*).

    Parameters
    ----------
    mobj : openravepy.KinBody
        Movable object
    placementClearance : float (optional)
        When checking for stable placements, a supporting face f will count as a stable
        placement only if the projection of the COM is *well* inside the supporting face. 
        If the peojected point is closer to any edge of f than placementClearance, the face f
        is considered not stable.
    autoLoad : bool (optional)
        Indicate if grasp database should be computed anew.

    """

    def __init__(self, mobj, placementClearance=0.01, autoLoad=True):
        self.mobj = mobj # movable object
        self.env = self.mobj.GetEnv()
        self.hullname = self.mobj.GetName() + "_hull"
        self._filename = os.path.join(_pymanipdir, self.mobj.GetKinematicsGeometryHash() + '.myobject.{0:03d}.pp'.format(int(placementClearance*100)))

        # Check if there is an original mesh model
        hasOriginalMesh = False
        try:
            objectname = self.mobj.GetName() + '.off'
            xmlfilename = pkg_resources.resource_filename('pymanip', os.path.join('models', objectname))
            self._mobjOriginal = self.env.ReadKinBodyXMLFile(xmlfilename)
            self.env.Add(self._mobjOriginal, True)
            hasOriginalMesh = True
        except:
            self._mobjOriginal = self.mobj

        self._placementClearance = placementClearance
        self._setRestingSurfaceTransform = False # indicate if self._Trest has been set
        self._Trest = None

        # First create a mesh of the object's convex hull and preprocess to obtain information
        self._cdmodel = orpy.databases.convexdecomposition.ConvexDecompositionModel(robot=self._mobjOriginal, padding=0.0)
        if not self._cdmodel.load():
            # Compute the convex hull of every link
            self._cdmodel.generate(convexHullLinks=[l.GetName() for l in self._mobjOriginal.GetLinks()])
            self._cdmodel.save()

        with self.mobj:
            self.mobj.SetTransform(np.eye(4))
            self._mobjOriginal.SetTransform(np.eye(4))
            self._CreateHull()
            self._PreprocessHullKinBody()
            self._CreateFaceAdjacencyGraph()
            # self._PreprocessEdgesInformation()

        # Next preprocess all boxes to obtain grasp database
        self._graspclassescomputationtime = None
        if not self.Load() or not autoLoad:
            ts = time.time()
            nominalTransformationSet = [(isurface, self.ComputeTObject([isurface, 0, 0, 0], Trest=np.eye(4), forRealObject=True)) for isurface in self.stablefacetindices]
            self._graspinfos = dict()
            for (linkIndex, link) in enumerate(self.mobj.GetLinks()):
                if link.GetGeometries()[0].GetType() == orpy.GeometryType.Box:
                    log.info("Extracting grasp information from {0}: link{1:02d} (box)".format(self.mobj.GetName(), linkIndex))
                    self._graspinfos[linkIndex] = BoxInfo(self.mobj, linkIndex, nominalTransformationSet)
                # elif link.GetGeometries()[0].GetType() == orpy.GeometryType.Cylinder:
                #     log.info("Extracting grasp information from {0}: link{1:02d} (cylinder)".format(self.mobj.GetName(), linkIndex))
                #     self._graspinfos[linkIndex] = CylinderInfo(self.mobj, linkIndex, nominalTransformationSet)
                else:
                    log.info("Skip link {0:02d}: {1}; type {2}".format(linkIndex, link.GetName(), link.GetGeometries()[0].GetType()))
                    # log.warn("{0}: link{1:02d} geometry unknown")
                    # raise ValueError
            te = time.time()
            self._graspclassescomputationtime = te - ts
        if hasOriginalMesh:
            self._mobjOriginal.Enable(False)
            self._mobjOriginal.SetVisible(False)


    def Save(self):
        import h5py
        log.info("Saving MyObject.graspinfos ({0}) to {1}".format(self.mobj.GetName(), self._filename))
        try:
            os.makedirs(os.path.split(self._filename)[0])
        except OSError:
            pass

        data = dict()
        data['placementClearance'] = self._placementClearance
        data['graspinfos'] = self._graspinfos
        pickle.dump(data, open(self._filename, 'w'), pickle.HIGHEST_PROTOCOL)
                        

    def Load(self):
        try:
            data = pickle.load(open(self._filename, 'r'))
            assert(data['placementClearance'] == self._placementClearance)
            self._graspinfos = data['graspinfos']
            return True
        except:
            return False

            
    @property
    def ag(self):
        """ag is an adjacency graph (networkx's graph). Each node represents a facet of the convex
        hull of the object. An edge exists between two nodes if the corresponding two facets are
        adjacent. Created when calling _CreateFaceAdjacencyGraph from class initialization.

        Note that there will be a node corresponding to a facet regardless of whether or not the
        facet is a stable facet.

        """
        return self._ag

    @property
    def agStable(self):
        """agStable is a graph (networkx's graph) whose nodes represent stable facets. Each node will have
        edges reaching every other node. Each edge will have its associated cost computed from the
        length of the shortest path (on the graph) from self._ag.
        """        
        return self._agStable
            
    @property
    def graspinfos(self):
        """graspinfos is a dictionary. Each key is a linkIndex. The corresponding value is graspinfo for the
        link.
        """
        return self._graspinfos

    @property
    def edges(self):
        """edges is a dictionary where each key is an edge index and the corresponding value is the
        edge pointed to by the index.
        """
        return self._edges

    @property
    def edgeRotationRanges(self):
        """edgeRotationRanges is a dictionary storing the admissible ranges of rotation about the
        edges when calculating placement on those edges. Note that since a range will
        always be of the form

                  range = [0, gamma] where 0 <= a <= np.pi,

        the value of edgeRotationRanges[iedge] will be the number gamma.

        Consider for example an edge common to facet0 and facet1. With alpha = 0, the
        calculated transformation will be resting on facet0. With alpha = gamma, the
        calculated transformation will be resting on facet1. With alpha anywhere in
        between 0 and gamma, the calculated transformation will be solely on the edge.

        """
        return self._edgeRotationRanges
    
    @property
    def facetedges(self):
        """facetedges is a dictionary where each key is a pair of adjacent facets and each value is 
        the edge (which is in turn a pair of vertices) associated with the facet pair. Note that for
        any two adjacent facets, there can be only one edge associated with them.
        """
        return self._facetedges
    
    @property
    def facets(self):
        """facets is a dictionary where each key is a facet index and the corresponding value is a
        list of indices of faces which constitute this facet.
        (facet is a group of coplanar and adjacent faces)
        """
        return self._facets

    @property
    def filename(self):
        """filename is in the form '<hash>.myobject.<clearance>.pp' where hash is obtained from
        OpenRAVE and clearane is specified in centimeters.
        """
        return self._filename

    @property
    def invfacets(self):
        """invfacets is an inverted dictionary of facets. Each key is a face index and the
        corresponding value is the facet that face belongs to.

        """
        return self._invfacets

    @property
    def placementClearance(self):
        """placementClearance is a threshold for filtering out close-to-unstable stable placements. We
        define close-to-unstable stable placements based on the minimum distance of the projected
        COM to any edge of the contact surface (of the convex hull). If the projected COM is nearer
        to the closest edge than placementClearance, we filter out that placement since it is prone
        to tipping.
        """
        return self._placementClearance
    
    @property
    def stablefacetindices(self):
        """stablefacetindices is a list of indices of facets which can serve as a stable placement.
        """
        return self._stablefacetindices
    
    @property
    def Tcom(self):
        """Tcom is the transformation of the frame attached to the object's COM described in the
        object's local frame. (The object's local frame is usually located at the COM of the first
        link of the object.)

        """
        return self._Tcom

    @property
    def TcomInv(self):
        return self._TcomInv

    @property
    def Tedges(self):
        """Tedges is a list of transformations of ref. frames attached to edges of the convexhull of
        the object.

        """
        return self._Tedges

    @property
    def Trest(self):
        """Trest is the transformation of the frame attached to the table's surface. Object
        placement parameters such as x, y, theta are expressed with respect to this frame.

        """
        return self._Trest

    @property
    def Tsurfaces(self):
        """Tsurfaces is a list of transformation matrices. Tsurfaces[i] is the transformation of the
        surface's frame attached to facet i described in the frame attached to the object's
        COM. 

        Note that:

        1. Tsurfaces contains transformations for all surfaces regardless of whether or not it is a
        stable placement.

        2. The surface's frame has its normal vector pointing outward.
        
        """
        return self._Tsurfaces
    

    def _CreateFaceAdjacencyGraph(self):
        self._ag = nx.Graph() # an undirected graph
        # Each node of the graph is a facet index
        self._ag.add_nodes_from(self._facets.keys())

        # self.facetedges is a dictionary where each key is a pair of adjacent facet and each value
        # is the edge associated with the facet pair. For any two adjacent facets, there can be only
        # one edge associated with them, right? (verification needed)
        self._facetedges = dict() # keeps record of common edges between a pair of facets
        
        # faList is a list of pair of adjacent *face* (not facet)
        faList = self.trimesh.face_adjacency
        for facePair in faList:
            facet0 = self._invfacets[facePair[0]]
            facet1 = self._invfacets[facePair[1]]

            if (facet0 == facet1):
                continue
            
            # The two adjaceny faces belong to different facet
            facetPair = (facet0, facet1)
            vlist0 = [] # a list of all vertex indices constituting facet0
            vlist1 = [] # a list of all vertex indices constituting facet1
            # Examine all faces constituting the facet
            for face in self._facets[facet0]:
                vlist0 = vlist0 + self.faces[face].tolist()
            vlist0 = set(vlist0) # remove duplicate vertices
            for face in self._facets[facet1]:
                vlist1 = vlist1 + self.faces[face].tolist()
            vlist1 = set(vlist1) # remove duplicate vertices

            vCommon = list(vlist0 & vlist1) # common vertices of the two facets
            try:
                assert(len(vCommon) == 2) # there should be only one edge in common
            except:
                log.info("Found {0} vertices on the common edge between facet {1} and facet {2}".\
                         format(len(vCommon), facet0, facet1))

                # Fix 1: check if there are vertices that are very close to each other (redundant)
                refinedVertices = [vCommon[0]]
                distanceThreshold = 0.001
                d2 = distanceThreshold**2
                for vtest in vCommon:
                    redundant = False
                    for v in refinedVertices:
                        diff = self.vertices[vtest] - self.vertices[v]
                        if np.dot(diff, diff) <= d2:
                            redundant = True
                            break
                    if not redundant:
                        refinedVertices.append(vtest)
                if len(refinedVertices) > 2:
                    # Since all vertices are on the same line, select a pair which has the
                    # most distance
                    vv = product(refinedVertices, refinedVertices)
                    maxdist = 0.0
                    for pair in vv:
                        v0, v1 = pair
                        dist = utils.norm(pair[0] - pair[1])
                        if dist > maxdist:
                            refinedVertices = [v0, v1]
                log.info("Removed {0} vertices which are too close to their neighbor(s)".\
                         format(len(vCommon) - len(refinedVertices)))
                vCommon = refinedVertices
            self._facetedges[facetPair] = vCommon

            # v0 and v1 are two vertices on the common edge
            v0 = self.vertices[vCommon[0]]
            v1 = self.vertices[vCommon[1]]
            
            # Locate the center of each facet
            vcenter0 = np.average([self.vertices[index] for index in vlist0], axis=0)
            vcenter1 = np.average([self.vertices[index] for index in vlist1], axis=0)

            """Let p0, respectively p1, be the closest point on the line joining v0 and v1 to vcenter0,
            respectively v1. The line joining v0 and v1 can be expressed by a parameterized equation

                      v(t) = v0 + t*(v1 - v0) for all t.

            p0 can then be described as
            
                      p0 = v0 + tp0*(v1 - v0)

            where tp0 = -(v0 - vcenter0)^T(v1 - v0)/norm(v1 - v0)^2. The same goes for p1.
            """
            vect = v1 - v0
            tp0 = -np.dot(v0 - vcenter0, vect)/np.dot(vect, vect)
            p0 = v0 + tp0*(vect)

            tp1 = -np.dot(v0 - vcenter1, vect)/np.dot(vect, vect)
            p1 = v0 + tp1*(vect)

            d = np.linalg.norm(p1 - p0)
            l0 = np.linalg.norm(vcenter0 - p0)
            l1 = np.linalg.norm(vcenter1 - p1)

            # gdist is the *squared* geodesic distance from vcenter0 to vcenter1. (It is the length
            # of the line joining vcenter0 and vcenter1 when unfolding the hull such that facet0 and
            # facet1 are on the same plane, connected by the edge.)
            # We defined it to be the squared of the number just to reduce computation.
            gdist = (l0*l0 + l1*l1 + d*d) + 2*d*d*l0*l1/((l0 + l1)**2)

            # This geodesic distance will serve an the weight associated with the edge connecting
            # facet0 and facet1 in the adjacency graph.
            self._ag.add_edge(facet0, facet1, weight=gdist)

        self._agStable = nx.Graph()
        self._agStable.add_nodes_from(self._stablefacetindices)
        for ifacet in self._stablefacetindices:
            for jfacet in self._stablefacetindices:
                if ifacet == jfacet:
                    continue

                if (((ifacet, jfacet) in self._agStable.edges()) or
                    ((jfacet, ifacet) in self._agStable.edges())):
                    continue
                
                w = nx.dijkstra_path_length(self._ag, source=ifacet, target=jfacet, weight='weight')

                # Give more weight if the two rotations are far apart.
                normali = self.planes[ifacet][0:3]
                normalj = self.planes[jfacet][0:3]
                diffangle = np.arccos(np.dot(normali, normalj))
                w *= diffangle
                
                self._agStable.add_edge(ifacet, jfacet, weight=w)

            

    def _CreateHull(self):
        """Create a new OpenRAVE KinBody which is a convex hull of self._mobjOriginal.

        """
        # Figure out where the COM is with respect to the object's frame
        com = np.zeros(3)
        totalM = 0.0
        passed = True
        # Try weighted averaging all the links' COMs
        for link in self._mobjOriginal.GetLinks():
            m = link.GetMass()
            if m <= 0:
                passed = False
                break
            com += m * link.GetGlobalCOM()
            totalM += m
        if not passed:
            # Some links have zero mass so the averaging does not work. Now just assign the COM
            # as the average of all vertices.
            trimesh = self.env.Triangulate(self._mobjOriginal)
            com = np.average(trimesh.vertices.tolist(), axis=0)
        else:
            com /= totalM
        
        self._Tcom = np.eye(4)
        self._Tcom[0:3, 3] = np.array(com)
        self._TcomInv = np.eye(4)
        self._TcomInv[0:3, 3] = -np.array(com) # now SetTransform(TcomInv) will make the object's COM
                                               # coincide with the world's origin        

        self._mobjOriginal.SetTransform(self._TcomInv)
        # Collect collision meshes of all links and combine them into one OpenRAVE TriMesh
        link = self._mobjOriginal.GetLinks()[0]
        geom = link.GetGeometries()[0]
        mesh = geom.GetCollisionMesh()
        maxindex = -1
        indices = mesh.indices + (maxindex + 1) # maxindex + 1 is an offset for indices
        # mesh.vertices are described in the local frame. So transform them into the global frame
        vertices = [utils.Transform(v, link.GetTransform()) for v in mesh.vertices]
        vertices = np.asarray(vertices)

        for link in self._mobjOriginal.GetLinks()[1:]:
            geom = link.GetGeometries()[0]
            mesh = geom.GetCollisionMesh()
            curindices = mesh.indices + (maxindex + 1)
            indices = np.vstack([indices, curindices])
            maxindex = curindices.max()
            curvertices = [utils.Transform(v, link.GetTransform()) for v in mesh.vertices]
            vertices = np.vstack([vertices, curvertices])                      
            
        # Create a new TriMesh object
        mesh = orpy.TriMesh(vertices, indices)
        # self.hull is a list of vertices and their corresponding indices
        self.hull = self._cdmodel.ComputePaddedConvexHullFromTriMesh(mesh, 0.0)
        # self.hullmesh is a trimesh of the hull
        self.hullmesh = self._cdmodel.GenerateTrimeshFromHulls([self.hull])
        
        self.hullkinbody = orpy.RaveCreateKinBody(self.env, '')
        self.hullkinbody.SetName(self.hullname)
        self.hullkinbody.InitFromTrimesh(self.hullmesh)


    def _PreprocessHullKinBody(self):
        """From the OpenRAVE hull kinbody, this function compute stable surface equations and
        other information related to object placements.

        """
        trimesh = tm.Trimesh(vertices=self.hull[0], faces=self.hull[1]) # using package trimesh
        self.trimesh = trimesh.convex_hull
        self.trimesh.fix_normals()
        self.vertices = self.trimesh.vertices
        self.faces = self.trimesh.faces
        self._facets = dict() # a facet is a group of coplanar faces.
        for (i, facet) in enumerate(self.trimesh.facets()):
            self._facets[i] = facet
            
        # There might also be some faces that are not coplanar with anyone else
        nfacets = len(self._facets)
        # `values` is the list of all faces being included in self._facets
        values = np.hstack(self._facets.values()).tolist() 
        for i in xrange(len(self.faces)):
            if i not in values:
                self._facets[nfacets] = [i] # create a new entry
                nfacets += 1

        self._invfacets = dict() # this tells us which facet the face index i belongs to
        for (ifacet, val) in self._facets.iteritems(): # iterating over (key, value) pair
            for iface in val:
                self._invfacets[iface] = ifacet

        # Now compute plane equations of all planes
        self.planes = [] # a list of all surface equations
        self.facet_vertices = dict() # a dictionary where each key is a facet index and the
                                     # corresponding values are vertices on that facet
        for (ifacet, facet) in self._facets.iteritems():
            indices = [] # a set containing non-redundant vertex indices of this facet
            for faceindex in facet:
                # Each face includes three vertices
                vi0, vi1, vi2 = self.faces[faceindex]
                if vi0 not in indices:
                    indices.append(vi0)
                if vi1 not in indices:
                    indices.append(vi1)
                if vi2 not in indices:
                    indices.append(vi2)

            self.facet_vertices[ifacet] = [self.vertices[i] for i in indices]
            # Don't forget that all vertices are expressed in the frame attached to the COM.

            # Assuming that all vertices are perfectly on one plane. We select three
            # vertices to compute the plane's equation.
            v0 = np.array(self.vertices[indices[0]])
            v1 = np.array(self.vertices[indices[1]])
            v2 = np.array(self.vertices[indices[2]])            
            
            # dv0 = v1 - v0
            # dv1 = v2 - v0
            # dv0xdv1 = np.cross(dv0, dv1)
            # length = np.linalg.norm(dv0xdv1)
            # normal = dv0xdv1/length

            # TODO: change the above code since it is not necessary to retrieve all the vertices
            normal = self.trimesh.face_normals[facet[0]]

            # An equation is of the form [A, b], describing a plane Ax = b.
            equation = np.hstack([normal, np.dot(normal, v0)])
            self.planes.append(equation)
        self.planes = np.vstack(self.planes)
                    
        # Find out which surface can provide a stable placement
        self._stablefacetindices = []
        self._Tsurfaces = []
        A = self.planes[:, 0:3]
        b = self.planes[:, 3]
        for (iplane, plane) in enumerate(self.planes):
            isStable = False
            normal = plane[0:3]
            offset = plane[3]
            point = offset * normal # the center of the object projected onto the plane of the surface
            
            if (np.all(np.around(np.dot(A, point) - b, decimals=6) <= 0)):
                # The projection lies inside of the hull. Now test if the point has some clearance
                # to the boundary.

                # Plane index and facet index are consistent (from construction)
                vertices = self.facet_vertices[iplane]

                # Now try to find edges of this supporting surface
                t1 = utils.GetPerpendicularVector(normal)
                t2 = np.cross(normal, t1)
                R = np.vstack([t1, t2, normal])
                # All vertices in v_projected has the same z-coordinate (from construction)
                v_projected = [np.dot(R, v) for v in vertices]
                v2d = [np.array([v[0], v[1]]) for v in v_projected] # 2d vertices
                hull = ConvexHull(v2d, qhull_options='E0.0001')
                # simplices is a list of edges (indices pointing to vertices) forming this 2d convex
                # hull
                simplices = hull.simplices # after all, this is what we need

                isSafe = True
                for edge in hull.simplices:
                    p0, p1 = [vertices[index] for index in edge]
                    # d is the distance from point to the 3d line connecting p0 and p1
                    d = np.linalg.norm(np.cross(p1 - p0, p0 - point))/np.linalg.norm(p1 - p0)
                    if d < self._placementClearance:
                        isSafe = False
                        break
                if isSafe:
                    self._stablefacetindices.append(iplane)
            
            z = normal # surface normal vector pointing out of the hull
            x = utils.GetPerpendicularVector(z)
            y = np.cross(z, x)
            
            # R describes the rotation of the surface frame. Each column is a local
            # axis (of the surface frame) described in the object's frame
            R = np.vstack([x, y, z]).T
            p = np.reshape(point, (3, 1))
            # p = np.average(self.facet_vertices[iplane], axis=0).reshape((3, 1))
            T = np.vstack([np.hstack([R, p]), np.array([0., 0., 0., 1.])])            
            self._Tsurfaces.append(T)


    def _PreprocessEdgesInformation(self):
        """From the trimesh object, gather information about edges which will be useful when computing
        placements when allowing edge placement (= the object is also allowed to be placed on its
        (convux hull's) edges).

        """
        # Compute all Tedges (= assigning a frame attached to each edge). Note that the following
        # procedure will introduce an implication that edge indexing is depending on how
        # facetedges.values() are arranged.
        self._Tedges = []

        self._edges = dict()
        self._edgeRotationRanges = dict()
        for (iedge, edge) in enumerate(self._facetedges.values()):
            self._edges[iedge] = edge
            
            facetPair = [key for (key, val) in self._facetedges.iteritems() if val == edge][0]
            ifacet0, ifacet1 = facetPair

            # Note that this sorted indices will provide the direction of the first axis for the
            # edge's frame
            i0, i1 = sorted(edge) # sort and unpack the vertex indices

            # v0 and v1 are coordinates of the vertices described in the frame attached to the COM.
            v0 = self.vertices[i0]
            v1 = self.vertices[i1]

            vMid = 0.5*(v0 + v1)
            firstAxis = utils.Normalize(v1 - v0)

            # Now we need to find out the second axis for this edge's frame.
            p0 = np.average(self.facet_vertices[ifacet0], axis=0) # the center of facet0
            p1 = np.average(self.facet_vertices[ifacet1], axis=0) # the center of facet1
            Tsurface0 = self._Tsurfaces[ifacet0]
            Tsurface1 = self._Tsurfaces[ifacet1]
            # p0 = utils.ExtractTranslationVector(Tsurface0)
            # p1 = utils.ExtractTranslationVector(Tsurface1)

            z0 = utils.Normalize(Tsurface0[0:3, 2]) # the normal vector pointing out of surface 0
            z1 = utils.Normalize(Tsurface1[0:3, 2]) # the normal vector pointing out of surface 1

            gamma = np.arccos(np.dot(z0, z1))
            self._edgeRotationRanges[iedge] = gamma

            temp0 = p0 - vMid
            thirdAxis0 = utils.Normalize(np.cross(firstAxis, temp0))
            thresh_dot = 1e-6
            dot1 = np.dot(z0, thirdAxis0)
            if dot1 > 1:
                assert(dot1 - 1 < thresh_dot)
                dot1 = 1
            elif dot1 < -1:
                assert(-1 - dot1 < thresh_dot)
                dot1 = -1
            if np.arccos(dot1) <= np.pi/180:
                # Found out that calculating p0, p1 by averaging the coordinates might
                # introduce some small numerical discrepancies. Therefore, here we allow a
                # small deviation of 1 degree.
                thirdAxis = thirdAxis0
                p = p0
            else:
                temp1 = p1 - vMid
                thirdAxis = utils.Normalize(np.cross(firstAxis, temp1))
                dot2 = np.dot(z1, thirdAxis)
                assert(abs(dot2 - 1) <= 1e-4)
                p = p1
            secondAxis = np.cross(thirdAxis, firstAxis)

            R = np.vstack([firstAxis, secondAxis, thirdAxis]).T
            Tedge = utils.CombineRotationTranslation(R, vMid)
            self._Tedges.append(Tedge)


    def ComputeTObject(self, qobj, Trest=None, forRealObject=True):
        """Here we parameterize an object placement by 4 numbers:
        
                   qobj = [isurface, x, y, theta]
        
        isurface indicates which stable surface (facet) the object is resting on. x, y, and theta
        are defined with respect to some nominal point in the scene (for example, the center of the
        table).

        """
        if (Trest is None) and (not self._setRestingSurfaceTransform):
            log.warn("Please set a resting surface transformation or input one")
            return None
        else:
            if Trest is None:
                Trest = self.Trest

        isurface, x, y, theta = qobj
        Tx = utils.ComputeTRot(pX, np.pi)
        # the surface frame's +Z is pointing *into* the resting surface
        Tz = utils.ComputeTRot(mZ, -theta)

        if forRealObject:
            Tobj = np.linalg.inv(np.dot(self.Tcom, self._Tsurfaces[isurface]))
        else:
            Tobj = np.linalg.inv(self._Tsurfaces[isurface])
        # Now Tobj makes the object surface's frame coincide with the world's
        # frame. (Therefore, the object is into the floor)

        Tobj = reduce(np.dot, [Tx, Tz, Tobj])

        # Apply the given translation
        Tp = np.eye(4)
        Tp[0:3, 3] = np.array([x, y, 0])
        Tobj = np.dot(Tp, Tobj)

        Tobj = np.dot(Trest, Tobj) # move the object onto the resting surface
        return Tobj


    def ComputeQObject(self, Tobj, Trest=None, forRealObject=True):
        """Given an object transformation (stable placement), try to deduce the four parameters:
        
                  qobj = [isurface, x, y, theta]

        The transformation of the resting surface must match with the one used to compute Tobj.

        From ComputeTObject, Tobj can be described as

                  Tobj = Trest*Tp*Tx*Tz*(Tcom*Tsurface)^-1
        """
        if (Trest is None) and (not self._setRestingSurfaceTransform):
            log.warn("Please set a resting surface transformation or input one")
            return None
        else:
            if Trest is None:
                Trest = self.Trest

        T = np.dot(np.linalg.inv(Trest), Tobj)
        # Next iterate through all Tsurface to find the answer
        found = False
        solindex = -1
        for (isurface, Tsurface) in enumerate(self._Tsurfaces):
            if forRealObject:
                temp = reduce(np.dot, [T, self.Tcom, Tsurface])
            else:
                temp = np.dot(T, Tsurface)

            """If this Tsurface is the answer, temp should have the form
            
            temp = [ c -s  0  x ]
                   [-s -c  0  y ]
                   [ 0  0 -1  0 ]
                   [ 0  0  0  1 ]

            """
            threshold = 1e-4
            if (abs(temp[0, 2]) < threshold and
                abs(temp[1, 2]) < threshold and
                abs(temp[2, 0]) < threshold and
                abs(temp[2, 1]) < threshold and
                abs(temp[2, 2] + 1) < threshold):
                found = True
                solindex = isurface
                x = temp[0, 3]
                y = temp[1, 3]

                if temp[0, 0] > 1 or temp[0, 0] < -1:
                    if abs(temp[0, 0]) - 1 <= threshold:
                        angle = np.arccos(np.sign(temp[0, 0]))
                    else:
                        raise Exception, "temp = np.{0}".format(repr(temp))
                else:
                    angle = np.arccos(temp[0, 0])
                if temp[1, 0] < 0:
                    # sine of the angle is positive, the calculated theta is safe.
                    theta = angle
                else:
                    theta = -angle
                break
        assert(found)
        return [solindex, x, y, theta]
                
                
    def SetRestingSurfaceTransform(self, T):
        """The input transformation T is the transformation of the resting surface's frame. The
        object placement parameters x, y, and theta are defined with respect to the
        resting surface's frame.

        Convention:
        - The origin of the surface frame (T[0:3, 3]) must be right on top of the surface
        - The +Z axis of the surface must be pointing outward

        """
        self._Trest = T
        self._setRestingSurfaceTransform = True


    def ComputeTObjectOnEdge(self, qobj, Trest=None, forRealObject=True):
        """Compute a transformation which places the object on its edge. qobj is a parameterization
        of this special placement classes of the form

        qobj = [iedge, alpha, x, y, theta].

        iedge is an index to the supporting edge.

        alpha is a normalized angle (being in [0, 1]). If alpha = 0, the object is resting on a
        supporting surface adjacent to the edge. If alpha = 1, the object is resting on another
        adjacent supporting surface. When 0 < alpha < 1, the object is placed on its edge.

        ** not sure if alpha should be a normalized one or not. We try first without normalization.

        In order for this to work, _PreprocessEdgesInformation has to be called first.

        """
        if (Trest is None) and (not self._setRestingSurfaceTransform):
            log.warn("Please set a resting surface transformation or input one")
            return None
        else:
            if Trest is None:
                Trest = self.Trest
                
        iedge, alpha, x, y, theta = qobj
        
        Tedge = self._Tedges[iedge]
        Trotx = utils.ComputeTRot(0, alpha)
        T = np.dot(Tedge, Trotx)
        if forRealObject:
            Tobj = np.linalg.inv(np.dot(self.Tcom, T))
        else:
            Tobj = np.linalg.inv(T)

        Tx = utils.ComputeTRot(pX, np.pi)
        Tz = utils.ComputeTRot(mZ, -theta) # the surface frame's +Z is pointing into the resting surface

        Tobj = reduce(np.dot, [Tx, Tz, Tobj])

        # Apply the given translation
        Tp = np.eye(4)
        Tp[0:3, 3] = np.array([x, y, 0])
        Tobj = np.dot(Tp, Tobj)

        Tobj = np.dot(Trest, Tobj) # move the object onto the resting surface
        return Tobj


    def ComputeQObjectOnEdge(self, Tobj, Trest=None, forRealObject=True):
        """Given an object transformation that places the object on one of its edge, try to deduce
        the five parameters:
        
                  qobj = [iedge, alpha, x, y, theta].

        The transformation of the resting surface must match with the one used to compute Tobj.

        From ComputeTObjectOnEdge, Tobj can be described as

                  Tobj = Trest*Tp*Tx*Tz*(Tcom*(Tedge*Talpha))^-1
        """
        if (Trest is None) and (not self._setRestingSurfaceTransform):
            log.warn("Please set a resting surface transformation or input one")
            return None
        else:
            if Trest is None:
                Trest = self.Trest

        T = np.dot(np.linalg.inv(Trest), Tobj)
        # Next iterate through all Tedges to find the answer
        found = False
        solindex = -1
        for (iedge, Tedge) in enumerate(self._Tedges):
            if forRealObject:
                temp = reduce(np.dot, [T, self.Tcom, Tedge])
            else:
                temp = np.dot(T, Tedge)

            """If this Tedge is the answer, temp should have the form

            temp = [ c  -s(ca)  -s(sa)  x]
                   [-s  -c(ca)  -c(sa)  y]
                   [ 0    sa      -ca   0]
                   [ 0     0        0   1]

            where c, s are cos(theta) and sin(theta), and ca, sa are cos(alpha) and sin(alpha).
            """
            c = temp[0, 0]
            s = -temp[1, 0]
            ca = -temp[2, 2]
            sa = temp[2, 1]
            threshold = 1e-4
            if (abs(temp[2, 0]) < threshold and
                abs(temp[2, 3]) < threshold and
                abs(s*ca + temp[0, 1]) < threshold and
                abs(c*ca + temp[1, 1]) < threshold and
                abs(s*sa + temp[0, 2]) < threshold and
                abs(c*sa + temp[1, 2]) < threshold):
                found = True
                solindex = iedge
                x = temp[0, 3]
                y = temp[1, 3]
                angle1 = np.arccos(c)
                if s > 0:
                    theta = angle1
                else:
                    theta = -angle1
                alpha = np.arcsin(sa)
                break
        assert(found)
        return [iedge, alpha, x, y, theta]

    
    def ComputeTGripper(self, qgrasp):
        """Compute the global gripper transformation such that the gripper is in a grasping
        position with grasp parameter qgrasp.
 
        """
        linkIndex = qgrasp[0]
        return self.graspinfos[linkIndex].ComputeTGripper(self.mobj, qgrasp)


    def SampleQGrasp(self, ifacet, approachingdir=None):
        """Sample a grasping configuration of the form
        
                qgrasp = [ibox, approachingdir, slidingdir, delta]
        
        ibox: an integer specifying which box the robot is grasping
        
        approachingdir: an integer specifying in which direction the gripper approaches 
                        the object. approachingdir is described in the form
                        appdir = 6*ibox + dir, dir ranges from 0 to 5.
        
        slidingdir: an integer specifying with which direction the sliding axix of the gripper
                    is aligned

        delta: a float specifying where along the box the gripper is grasping.
        
        """
        if approachingdir is None:
            hasApproachingDir = False
            ibox = None
            boxinfo = None
            realapproachingdir = None
            while not hasApproachingDir:
                ibox = _rng.choice(self.graspinfos.keys())
                boxinfo = self.graspinfos[ibox]
                try:
                    realapproachingdir = _rng.choice(boxinfo.approachingDirections[ifacet])
                    hasApproachingDir = True
                except IndexError:
                    continue
            approachingdir = 6*ibox + realapproachingdir
        else:
            ibox = int(approachingdir)/6
            boxinfo = self.graspinfos[ibox]
            realapproachingdir = np.mod(approachingdir, 6)
        
        slidingdir = _rng.choice(boxinfo.slidingDirections[realapproachingdir])
        while (ifacet, realapproachingdir, slidingdir) not in boxinfo.intervals:
            slidingdir = _rng.choice(boxinfo.slidingDirections[realapproachingdir])
        delta = utils.WeightedChoice2(boxinfo.intervals[ifacet, realapproachingdir, slidingdir])
        qgrasp = [ibox, approachingdir, slidingdir, delta]
        return qgrasp


    def SetTransform(self, T):
        """Set self.mobj such that the COM has the transform T.
        """
        # Treal is the actual transform to achieve the effect we want
        Treal = np.dot(T, self._TcomInv)
        self.mobj.SetTransform(Treal)
    
