import numpy as np
from toppso3 import lie
from TOPP import Trajectory
from openravepy import planningutils as orutils
from openravepy import RaveCreateTrajectory
import pickle

epsilon = 1e-8

class TransitTrajectory(object):
    
    def __init__(self, lTraj=None, rTraj=None, lFirst=True):
        """TransitTrajectory stores (at most) two trajectories, for left and right robots. Note
        that there is no guarantee that when executing the two trajectories
        simultaneously, the robots will not collide.

        Normally, the execution order should be left -> right. This is specified by
        lFirst. If the transit traj has lFirst equal to True, when reversing the
        trajectory, lFirst will be False, i.e., rTraj should be executed first.

        Parameters
        ----------
        lTraj : openravepy.Trajectory
            Trajectory for left robot
        rTraj : openravepy.Trajectory
            Trajectory for right robot
        lFirst : bool
            Indicate if lTraj is to be executed first or not.

        """
        assert(not (lTraj is None and rTraj is None))
        # if lTraj is not None and rTraj is not None:
        #     assert(abs(lTraj.GetDuration() - rTraj.GetDuration()) <= epsilon)
        # if lTraj is not None:
        #     self.duration = lTraj.GetDuration()
        # else:
        #     self.duration = rTraj.GetDuration()

        self.lTraj = lTraj
        self.rTraj = rTraj
        self.lFirst = lFirst


    def Reverse(self):
        """
        Reverse the trajectory(-ies) stored in self. Do not return anything.
        """
        self.lFirst = not self.lFirst
        lTrajRev = orutils.ReverseTrajectory(self.lTraj)
        rTrajRev = orutils.ReverseTrajectory(self.rTraj)
        self.lTraj = lTrajRev
        self.rTraj = rTrajRev
        # not sure if we need to do retiming here.


    def Save(self, filename):
        """Save this transit trajectory to file.

        """
        obj = []
        if self.lTraj is not None:
            obj.append(self.lTraj.serialize())
        else:
            obj.append(None)

        if self.rTraj is not None:
            obj.append(self.rTraj.serialize())
        else:
            obj.append(None)
            
        obj.append(self.lFirst)
        try:
            with open(filename, 'wb') as f:
                pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
            return True
        except:
            print "Exception occured when saving the trajectory to file: {0}".format(filename)
            return False

    @staticmethod
    def Load(filename, env):
        """Load a transit trajectory from file.

        """
        try:
            with open(filename, 'rb') as f:
                obj = pickle.load(f)
        except:
            return None

        if obj[0] is not None:
            lTraj = RaveCreateTrajectory(env, '')
            lTraj.deserialize(obj[0])
        else:
            lTraj = None

        if obj[1] is not None:
            rTraj = RaveCreateTrajectory(env, '')
            rTraj.deserialize(obj[1])
        else:
            rTraj = None
        return TransitTrajectory(lTraj, rTraj, obj[2])

        
    def _Serialize(self):
        obj = []
        if self.lTraj is not None:
            obj.append(self.lTraj.serialize())
        else:
            obj.append(None)

        if self.rTraj is not None:
            obj.append(self.rTraj.serialize())
        else:
            obj.append(None)
            
        obj.append(self.lFirst)
        return pickle.dumps(obj)

    @staticmethod
    def _Deserialize(pp, env):
        obj = pickle.loads(pp)
        if obj[0] is not None:
            lTraj = RaveCreateTrajectory(env, '')
            lTraj.deserialize(obj[0])
        else:
            lTraj = None

        if obj[1] is not None:
            rTraj = RaveCreateTrajectory(env, '')
            rTraj.deserialize(obj[1])
        else:
            rTraj = None
        return TransitTrajectory(lTraj, rTraj, obj[2])
        

class ManipulationTrajectory(object):
    TRANSIT  = 0
    TRANSFER = 1 # closed-chain

    def __init__(self, trajsList=None, trajTypesList=None):
        self.trajsList = trajsList if trajsList is not None else []
        self.trajTypesList = trajTypesList if trajTypesList is not None else []


    def __getitem__(self, i):
        return self.trajsList[i]
        

    def AddTrajectory(self, traj, trajType):
        self.trajsList.append(traj)
        self.trajTypesList.append(trajType)
        

    def AddManipTrajectory(self, manipTraj):
        self.trajsList += manipTraj.trajsList
        self.trajTypesList += manipTraj.trajTypesList


    def Save(self, filename):
        """Save this manipulation trajectory to file.

        """
        trajsList = []
        for (traj, trajType) in zip(self.trajsList, self.trajTypesList):
            if trajType == self.TRANSFER:
                trajsList.append(traj)
            else:
                # TRANSIT trajectory needs to be serialized first.
                trajsList.append(traj._Serialize())
        try: 
            with open(filename, 'wb') as f:
                pickle.dump([trajsList, self.trajTypesList], f, pickle.HIGHEST_PROTOCOL)
            return True
        except:
            print "Exception occured when saving the result"
            return False

    @staticmethod
    def Load(filename, orenv):
        try:
            with open(filename, 'rb') as f:
                trajsList_, trajTypesList = pickle.load(f)
        except:
            print "Exception occured when loading file: {0}".format(filename)
            return None
        trajsList = []
        for (traj, trajType) in zip(trajsList_, trajTypesList):
            if trajType == ManipulationTrajectory.TRANSFER:
                trajsList.append(traj)
            else:
                transitTraj = TransitTrajectory._Deserialize(traj, orenv)
                trajsList.append(transitTraj)
        return ManipulationTrajectory(trajsList, trajTypesList)
                

        
