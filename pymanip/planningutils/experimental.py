"""
Functions in this file are purely experimental.
"""
import openravepy as orpy
import numpy as np
from copy import deepcopy
import scipy as sp
from scipy.spatial import ConvexHull
from cdd import RepType
from cone import FractionToFloatArray
import cone
import cvxopt

import hullobject
import trimesh
import utils

# Important notes: stability result differs by pretty much when changing this _decimals. Part of it
# may be because we are mainly using 'float' option when computing cdd (faster but less precise).
_decimals = 10 # for np.around

##########################################################################################
# Placement-related

# TODO: ensure the feasibility of the transformations returned by this function. (taking into
# account robot/ grasping/ etc.)
def ComputeClosestPlacement(hObject, Tcur, Trest, considerEdges=True):
    """Given a current transformation of hObject and the resting surface transformation Trest, find the
    closest transformation Tplaced for the object such that the object is resting on the resting
    surface.

    Here resting means either an *edge* or a *surface* of the object touches the floor (described by
    Trest).

    """
    # Compute the relative transformation from the resting surface to the object's frame
    Trel = np.dot(np.linalg.inv(Trest), Tcur)

    # We create a new mesh for computing AABB which is aligned with the resting surface's frame
    newVertices = [utils.Transform(v, Trel) for v in hObject.vertices]

    """
    # script for adding an AABB kinbody to the environment
    
    newMesh = trimesh.Trimesh(vertices=newVertices, faces=hObject.faces)
    newMesh.fix_normals()
    aabbExtents = newMesh.bounding_box.extents # dimension of the AABB
    bb = orpy.RaveCreateKinBody(env, '')
    bb.InitFromBoxes(np.array([[0, 0, 0, aabbExtents[0]/2, aabbExtents[1]/2, aabbExtents[2]/2]]))
    bb.SetName('bb')
    bb.GetLinks()[0].GetGeometries()[0].SetTransparency(0.5)
    env.Add(bb)
    """

    # Sort newVertices according to the Z-component of each vertex (to find out which one is
    # touching the floor first)
    sortedcoords, sortedindices = (list(t) for t in zip(*sorted(zip([v[2] for v in newVertices], range(len(newVertices))))))

    # For sure the following vertex must be touching the ground
    index0 = sortedindices[0]

    # Here we look for 2 edges which are the closest to the floor. Let alpha1 and alpha2 be the
    # angles between edge1 and the floor, and edge2 and the floor. If alpha1 and alpha2 are close to
    # each other (edges are equally near the floor), we assume that we can just place the object on
    # the facet which has edge1 and edge2 as its edges. Otherwise, we choose to place the object on
    # the edge nearest to the floor.

    # Probably starting by finding the closest face is easier
    candidateFacets = []
    for (ifacet, faces) in hObject.facets.iteritems():
        # Collect all vertices forming this facet
        allVertices = []
        for iface in faces:
            allVertices += hObject.faces[iface].tolist()

        if index0 in allVertices:
            candidateFacets.append(ifacet)
    assert(len(candidateFacets) > 0)

    # Find out the facet closest to the floor
    minAngle = np.pi
    closestFacet = -1
    MZ = np.array([0, 0, -1])
    for ifacet in candidateFacets:
        normal = utils.Rotate(hObject.planes[ifacet][0:3], Trel)
        angle = np.arccos(np.dot(normal, MZ))
        if abs(angle) < minAngle:
            minAngle = angle
            closestFacet = ifacet
    assert(closestFacet >= 0)
    
    # Find out if we should place the object on its edge or its facet
    if considerEdges:
        pass

    # For now let's skip the placing on edges.
    normal = utils.Rotate(hObject.planes[closestFacet][0:3], Trel)
    # Then compute a close object configuration at which the normal is aligned with -Z
    # see http://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d

    v = np.cross(normal, MZ)
    c = np.dot(normal, MZ) # cosine of the angle between the two
    vx = utils.ComputeSkewFromVect(v)
    R = np.eye(3) + vx + (1./(1 + c))*np.dot(vx, vx)
    p = np.zeros(3)
    T = utils.CombineRotationTranslation(R, p)

    # Tint is an intermediate transformation where the closest facet is already parallel to the floor
    Tcur_rotation = utils.ExtractRotation(Tcur)
    tcur = Tcur[0:3, 3] # translational part
    Tint = np.dot(T, Tcur_rotation) # when composing transformations, ignore translational part first
    Tint[0:3, 3] = tcur # put back the translational part
    
    Ttrans = utils.ComputeTTrans(2, -(Tint[2][3] - hObject.planes[closestFacet][3]))
    Tfinal = np.dot(Ttrans, Tint)

    return Tfinal


##########################################################################################
# Static Equilibrium

def GetContactPoints(robot, mobj, qgrasp, computeLocal=True, TcomLocal=None):
    """Return a list of 2 sublists. Each sublist contains contact points between one fingertip and the
    object. qgrasp is need to detemine the planes that contact points are lying on. The argument
    computeLocal indicates if the function should return contact points described in the glbal frame
    or the local frame.

    ** Local frame refers to a frame attached to the object's COM which might not necessarily
       coincide with the object's frame.

    Return lhullPoints, rhullPionts, Rl, Rr

    lhullPoints and rhullPoints are filtered contact points.

    Rl and Rr are rotation matrix describing the surfaces' frame --> the last column of each matrix
    represents a normal vector pointing out of the corresponding contact surface. The first two
    columns describe the remaining axes (parallel to the surface).

    Assumption:

    1. The robot must already be grabbing (in contact with) the object.

    2. robot is equipped with a parallel jaw gripper. Therefore, there # are only two parallel
    contact surfaces.

    """
    lfingertipname = 'robotiq_fingertip_l'
    rfingertipname = 'robotiq_fingertip_r'

    # If the robot is already grabbing the object, need to ungrab first.
    isGrabbing = robot.IsGrabbing(mobj) is not None
    if isGrabbing:
        robot.Release(mobj)

    # Change the collision checker to pqp in order to be able to obtain contact points.
    lcolRep = orpy.CollisionReport()
    rcolRep = orpy.CollisionReport()
    env = robot.GetEnv()
    prevChecker = env.GetCollisionChecker()
    usingNewChecker = not (prevChecker.GetXMLId() == 'pqp')
    if usingNewChecker:
        env.SetCollisionChecker(orpy.RaveCreateCollisionChecker(env, 'pqp'))

    env.CheckCollision(robot.GetLink(lfingertipname), mobj, report=lcolRep)
    env.CheckCollision(robot.GetLink(rfingertipname), mobj, report=rcolRep)
    if usingNewChecker:
        env.SetCollisionChecker(prevChecker)

    # Extract all detected contact points
    lpoints = [c.pos for c in lcolRep.contacts]
    rpoints = [c.pos for c in rcolRep.contacts]
    if len(lpoints) == 0:
        raise Exception("No contact detected on the left fingertip")
    if len(rpoints) == 0:
        raise Exception("No contact detected on the right fingertip")

    # Restore the grabbing state
    if isGrabbing:
        robot.Grab(mobj)

    # Preprocess contact points
    approachingDir, slidingDir, delta = qgrasp[-3:] # unpack data
    realApproachingDir = np.mod(approachingDir, 6)
    ibox = int(approachingDir/6)
    link = mobj.GetLinks()[ibox]
    extents = link.GetGeometries()[0].GetBoxExtents()
    if np.mod(realApproachingDir, 3) == 0:
        # Approaching direction is +X/-X
        if slidingDir == 1:
            extent = extents[2]
            grippingDir = 2
        elif slidingDir == 2:
            extent = extents[1]
            grippingDir = 1
    elif np.mod(realApproachingDir, 3) == 1:
        # Approaching direction is +Y/-Y
        if slidingDir == 0:
            extent = extents[2]
            grippingDir = 2
        elif slidingDir == 2:
            extent = extents[0]
            grippingDir = 0
    elif np.mod(realApproachingDir, 3) == 2:
        # Approaching direction is +Y/-Y
        if slidingDir == 0:
            extent = extents[1]
            grippingDir = 1
        elif slidingDir == 1:
            extent = extents[0]
            grippingDir = 0

    # Create a convex hull of all contact points (for removing redundant points). We need only the
    # contact points that are vertices of the contact surface.

    # We first transform all points into the link's frame because in the link's frame, there will be
    # one coordinate which is (approximately) constant.
    Tlink = link.GetTransform()
    TlinkInv = np.linalg.inv(Tlink)
    lLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in lpoints]
    rLinkLocalPoints = [utils.Transform(p, TlinkInv) for p in rpoints]

    # All the points (in the link's local frame) are coplanar. The plane is actuall described by
    # grippingDir. Due to some numerical error, we compute the average of that *constant*
    # coordinate.    
    lAvgCoord = np.average([p[grippingDir] for p in lLinkLocalPoints])
    rAvgCoord = np.average([p[grippingDir] for p in rLinkLocalPoints])

    # Process the left contact surface
    lLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in lLinkLocalPoints]
    lHull = ConvexHull(lLinkLocalPoints2d, qhull_options='E0.0001')
    lHullLocalPoints2d = [lHull.points[i].tolist() for i in lHull.vertices] # a set of non-redundant points
    lHullLocalPoints = deepcopy(lHullLocalPoints2d)
    for point in lHullLocalPoints:
        point.insert(grippingDir, lAvgCoord)
    lHullLocalPoints = [np.asarray(p) for p in lHullLocalPoints]
    
    # Process the right contact surface
    rLinkLocalPoints2d = [np.asarray([p[i] for i in xrange(3) if not (i == grippingDir)]) for p in rLinkLocalPoints]
    rHull = ConvexHull(rLinkLocalPoints2d, qhull_options='E0.0001')
    rHullLocalPoints2d = [rHull.points[i].tolist() for i in rHull.vertices] # a set of non-redundant points
    rHullLocalPoints = deepcopy(rHullLocalPoints2d)
    for point in rHullLocalPoints:
        point.insert(grippingDir, rAvgCoord)
    rHullLocalPoints = [np.asarray(p) for p in rHullLocalPoints]

    # Now transform all filtered points back into the global frame
    lPoints = [utils.Transform(p, Tlink) for p in lHullLocalPoints]
    rPoints = [utils.Transform(p, Tlink) for p in rHullLocalPoints]

    # Now we compute the description of the contact surface's frame (= obtain normal and tangential
    # components). We make use of the information of the gripper's transformation.
    Tmanip = robot.GetActiveManipulator().GetTransform()
    Rmanip = utils.ExtractRotationMatrix(Tmanip)

    # Rl is the rotation matrix describing the contact surface's frame of the surface touchuing the
    # gripper's left finger tip. The last column represent the normal vector of the surface's frame.
    # Since we are considering the free body diagram of the object, the normal vector should be
    # pointing *inward* (toward the object).

    # The following calculations are based on the current gripper configuration (denso + robotiq
    # gripper). If the robot changes, we also need to modify this.
    Rl = np.eye(3)
    Rl[0:3, 0] = Rmanip[0:3, 0]
    Rl[0:3, 1] = -Rmanip[0:3, 2]
    Rl[0:3, 2] = Rmanip[0:3, 1]

    Rr = np.eye(3)
    Rr[0:3, 0] = -Rmanip[0:3, 0]
    Rr[0:3, 1] = -Rmanip[0:3, 2]
    Rr[0:3, 2] = -Rmanip[0:3, 1]

    # Now lPoints, rPoints, Rl, Rr are all described in the global frame.
    if not computeLocal:
        return [lPoints, rPoints, Rl, Rr]

    # Here we need to return local descriptions of everything we have. Important note: the local
    # description is with respect to the frame attached to the COM (which might not coincide with
    # the object's local frame!!!)
    if TcomLocal is not None:
        Tcom = np.dot(mobj.GetTransform(), TcomLocal)
    else:
        Tcom = mobj.GetTransform()
    TcomInv = np.linalg.inv(Tcom)

    lLocalPoints = [utils.Transform(p, TcomInv) for p in lPoints]
    rLocalPoints = [utils.Transform(p, TcomInv) for p in rPoints]
    RlLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), Rl)
    RrLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), Rr)
    return [lLocalPoints, rLocalPoints, RlLocal, RrLocal]


def ComputeLocalGraspMatrix(localSurfaceContactPoints):
    G = []
    for point in localSurfaceContactPoints:
        G.append(np.vstack([np.eye(3), utils.ComputeSkewFromVect(point)]))
    return np.hstack(G)


def ComputeFaceRepresentation(nContactPoints, mu, R, fmax):
    """Compute the matrix Usurf.

    mu is the friction coefficient of the contact surface

    R is a rotation matrix representing the surface's frame (in the object's frame). The last column
    of R is the normal vector pointing out of the object's surface

    fmax is the maximum gripping force that the gripper can exert.

    """
    normal = R[0:3, 2]
    t1 = R[0:3, 0]
    t2 = R[0:3, 1]

    Upoint_i = np.vstack([-normal,
                          -t1 - mu*normal,
                           t1 - mu*normal,
                          -t2 - mu*normal,
                           t2 - mu*normal])
    
    ULastRow = np.tile(normal, nContactPoints)

    U = [Upoint_i for _ in xrange(nContactPoints)]
    Upoint = sp.linalg.block_diag(*U)
    Upoint = np.vstack([Upoint, ULastRow])
    Upoint = np.around(Upoint, decimals=_decimals)

    F = np.zeros(5*nContactPoints + 1)
    F[-1] = fmax
    return [Upoint, F]


def ComputeSpanRepresentation(robots, qgrasps, mobj, Tobj, mu, fmax, TcomLocal=None):
    """Given a set of robots, qgrasps, and the object, compute the matrices describing the span
    representation of WGI:
    
              WGI \in span(Astance*Vall)

    This function returns two lists: A_all and Vsurf_all. If there is no other contact points,
    Astance can be computed from Astance = hstack(A_all) and Vall can be computed from Vall =
    diag(Vsurf_all).

    1. A_all is a horizontal concatenation of A (representing relative transformations between the
    grapsed link and the COM).

    2. Vsurf_all is a list of Vsurf_i.

    This function will be used in functions for testing static equilibrium in different cases.

    Input arguments:
    
    robots: a list of robots
    
    qgrasps: a list of qgrasps. Each qgrasp is of the form qgrasp = [ibox, approachinDir,
             slidingDir, Delta].
    
    TcomLocal: a transformation matrix representing the relative transformation between the frame
               attached to the COM and the object's real local frame (which is normally attached to
               the first link).

    """
    # This matrix A will be put in Astance
    A = np.eye(6)
    if TcomLocal is not None:
        Tcom = np.dot(Tobj, TcomLocal)
    else:
        Tcom = Tobj
    R = utils.ExtractRotationMatrix(Tcom)
    p = Tcom[0:3, 3]
    p_hat = utils.ComputeSkewFromVect(p)
    A[0:3, 0:3] = -R
    A[3:6, 0:3] = -np.dot(p_hat, R)
    A[3:6, 3:6] = -R

    Vsurf_all = []
    A_all = []
    for (robot, qgrasp) in zip(robots, qgrasps):
        lpoints, rpoints, Rl, Rr = GetContactPoints(robot, mobj, qgrasp, computeLocal=True, TcomLocal=TcomLocal)
        Gl = ComputeLocalGraspMatrix(lpoints) # grasp matrix for the left contact surface
        Gr = ComputeLocalGraspMatrix(rpoints) # grasp matrix for the right contact surface
        # A valid contact force, f_all, is satisfying Upoint*f_all <= F.
        Upointl, Fl = ComputeFaceRepresentation(len(lpoints), mu, Rl, fmax)
        try:
            Vpointl_VREP = cone.FaceToSpan(Upointl, Fl, 'float')
        except RuntimeError:
            Vpointl_VREP = cone.FaceToSpan(Upointl, Fl, 'fraction')
            Vpointl_VREP = cone.FractionToFloatArray(Vpointl_VREP)
        Vpointl = cone.ExtractMatrix(Vpointl_VREP, RepType.GENERATOR)
        Vsurfl = np.dot(Gl, Vpointl)
        Vsurf_all.append(Vsurfl)
        A_all.append(A)

        Upointr, Fr = ComputeFaceRepresentation(len(rpoints), mu, Rr, fmax)
        try:
            Vpointr_VREP = cone.FaceToSpan(Upointr, Fr, 'float')
        except RuntimeError:
            Vpointr_VREP = cone.FaceToSpan(Upointr, Fr, 'fraction')
            Vpointr_VREP = cone.FractionToFloatArray(Vpointr_VREP)
        Vpointr = cone.ExtractMatrix(Vpointr_VREP, RepType.GENERATOR)
        Vsurfr = np.dot(Gr, Vpointr)
        Vsurf_all.append(Vsurfr)
        A_all.append(A)

    return [A_all, Vsurf_all]
        

def TestStaticEquilibrium(robots, qgrasps, mobj, Tobj, mu, fmax, TcomLocal=None):
    """Assumption:

    1. Robots are already set at their grasping position, the object as well.
    
    2. If the COM of the object is not located at the origin of the object's frame, the relative
    transformation (the transformation of the COM described in the object's frame) should be
    provided via TcomLocal).
    """

    A_all, Vsurf_all = ComputeSpanRepresentation(robots, qgrasps, mobj, Tobj, mu, fmax, TcomLocal)
    
    if TcomLocal is not None:
        Tcom = np.dot(Tobj, TcomLocal)
    else:
        Tcom = Tobj
    p = Tcom[0:3, 3]

    Astance = np.hstack(A_all)
    Vall = sp.linalg.block_diag(*Vsurf_all)

    # Round up the matrices
    AV = np.dot(Astance, Vall)
    AV = np.around(AV, decimals=_decimals)
    try:
        Ustance_HREP = cone.SpanToFace(AV, 1, 'float')
        B, Ustance = cone.ExtractMatrix(Ustance_HREP, RepType.INEQUALITY)
    except (RuntimeError, IndexError):
        Ustance_HREP = cone.SpanToFace(AV, 1, 'fraction')
        Ustance_HREP = cone.FractionToFloatArray(Ustance_HREP)
        B, Ustance = cone.ExtractMatrix(Ustance_HREP, RepType.INEQUALITY)

    env = robots[0].GetEnv()
    g = env.GetPhysicsEngine().GetGravity()
    m = 0
    for link in mobj.GetLinks():
        m += link.GetMass()
    WGI = np.hstack([m*g, m*np.cross(p, g)])

    # import IPython; IPython.embed()
    
    # If WGI is a valid wrench (i.e. the configuration is in static equilibrium), it should be in
    # the cone described by (Ustance, B).
    return np.alltrue(np.dot(Ustance, WGI) <= B)


def TestStaticEquilibriumOnSurface(robots, qgrasps, mobj, Tobj, mu, fmax, hObject, TcomLocal=None):
    pass


def TestStaticEquilibriumOnEdge(robots, qgrasps, mobj, Tobj, qobj, mu, fmax, hObject):
    iedge, alpha, x, y, theta = qobj # unpack qobj

    TcomLocal = hObject.Tcom
    Tcom = np.dot(Tobj, TcomLocal)
    p = Tcom[0:3, 3]
    TcomInv = np.linalg.inv(Tcom)

    env = robots[0].GetEnv()
    g = env.GetPhysicsEngine().GetGravity()
    m = 0
    for link in mobj.GetLinks():
        m += link.GetMass()
    WGI = np.hstack([m*g, m*np.cross(p, g)])
        
    A_all, Vsurf_all = ComputeSpanRepresentation(robots, qgrasps, mobj, Tobj, mu, fmax, TcomLocal)
    
    # Consider contacts due to placement
    edge = hObject.edges[iedge]
    i0, i1 = sorted(edge)
    v0 = hObject.vertices[i0]
    v1 = hObject.vertices[i1]
    R = np.eye(3) # the rotation matrix of this contact frame
    R[0:3, 0] = utils.Normalize(v1 - v0)
    R[0:3, 2] = np.array([0, 0, 1]) # assume for now that the supporting surface is horizontal
    R[0:3, 1] = np.cross(R[0:3, 2], R[0:3, 0])
    # The current R that we have is still described in the world's frame
    
    RLocal = np.dot(utils.ExtractRotationMatrix(TcomInv), R)
    
    G = ComputeLocalGraspMatrix([v0, v1])
    Upoint, F = ComputeFaceRepresentation(2, mu, RLocal, np.linalg.norm(m*g))
    Vpoint_VREP = cone.FaceToSpan(Upoint, F, 'float')
    Vpoint = cone.ExtractMatrix(Vpoint_VREP, RepType.GENERATOR)
    Vsurf = np.dot(G, Vpoint)
    Vsurf_all.append(Vsurf)

    A = np.eye(6)
    R = utils.ExtractRotationMatrix(Tcom)
    p_hat = utils.ComputeSkewFromVect(p)
    A[0:3, 0:3] = -R
    A[3:6, 0:3] = -np.dot(p_hat, R)
    A[3:6, 3:6] = -R
    A_all.append(A)
    
    ###
    Astance = np.hstack(A_all)
    Vall = sp.linalg.block_diag(*Vsurf_all)

    # Round up the matrices
    AV = np.dot(Astance, Vall)
    AV = np.around(AV, decimals=_decimals)
    try:
        Ustance_HREP = cone.SpanToFace(AV, 1, 'float')
        B, Ustance = cone.ExtractMatrix(Ustance_HREP, RepType.INEQUALITY)
    except (RuntimeError, IndexError):
        Ustance_HREP = cone.SpanToFace(AV, 1, 'fraction')
        Ustance_HREP = cone.FractionToFloatArray(Ustance_HREP)
        B, Ustance = cone.ExtractMatrix(Ustance_HREP, RepType.INEQUALITY)    

    # import IPython; IPython.embed()
    
    # If WGI is a valid wrench (i.e. the configuration is in static equilibrium), it should be in
    # the cone described by (Ustance, B).
    return np.alltrue(np.dot(Ustance, WGI) <= B)
