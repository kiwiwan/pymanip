import openravepy as orpy
import numpy as np
import time
import IPython
import random
rng = random.SystemRandom()

from bimanual.planners import cc_planner as ccp
from pymanip.planningutils import utils, myobject
ikfilter = orpy.IkFilterOptions.CheckEnvCollisions

"""
Test the BQuerySolver
"""
# Environment initialization
viewername = 'qtosg'
# viewername = 'qtcoin'
collisioncheckername = 'fcl_'
env = orpy.Environment()
# env.SetViewer(viewername)
# vw = env.GetViewer()
cc = orpy.RaveCreateCollisionChecker(env, collisioncheckername)
cc.SetCollisionOptions(orpy.CollisionOptions.ActiveDOFs)
env.SetCollisionChecker(cc)

# Robots
from os import path
robotpath = path.expanduser('~') + '/git/bimanual/xml/robots/'
robotfilename = 'denso_ft_gripper_with_base.robot.xml'
manipulatorname = 'denso_ft_sensor_gripper'
lrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
rrobot = env.ReadRobotXMLFile(robotpath + robotfilename)
lrobot.SetName('lrobot')
rrobot.SetName('rrobot')
env.Add(lrobot)
env.Add(rrobot)
lrobot.SetActiveDOFs(xrange(6))
rrobot.SetActiveDOFs(xrange(6))

# Relative transformation of the right_robot w.r.t the left_robot. (from calibration)
Tleft_right = \
np.array([[ 0.9999599222893029,   -0.0040494323330103115, 0.007142558327789952, -0.001071647451604687],
          [ 0.004003843532690571,  0.999975285228445,     0.005714046662232913,  1.0708242599889104  ],
          [-0.008007687065380032, -0.0056852199703562875, 0.9999581658905917,   -0.003974407767300958],
          [ 0.0,                   0.0,                   0.0,                   1.0                 ]])
rrobot.SetTransform(Tleft_right)

# Supporting surface
modelspath = '../pymanip/models/'
supporting_surface = env.ReadKinBodyXMLFile(modelspath + 'supporting_surface.kinbody.xml')
env.Add(supporting_surface)
Trest = np.array([[ 1.,  0.,  0.,  0.5*Tleft_right[0, 3]],
                  [ 0.,  1.,  0.,  0.5*Tleft_right[1, 3]],
                  [ 0.,  0.,  1., -0.155],
                  [ 0.,  0.,  0.,  1.]])
supporting_surface.SetTransform(Trest)
surface_frame = utils.CreateReferenceFrameKinBody(env, T=Trest, h=0.2, r=0.005)

## Setting up manipulators
ik6d = orpy.IkParameterization.Type.Transform6D
lmanip = lrobot.SetActiveManipulator(manipulatorname)
rmanip = rrobot.SetActiveManipulator(manipulatorname)
ltaskmanip = orpy.interfaces.TaskManipulation(lrobot)
rtaskmanip = orpy.interfaces.TaskManipulation(rrobot)
lcontroller = lrobot.GetController()
rcontroller = rrobot.GetController()
ik6d = orpy.IkParameterization.Type.Transform6D
likmodel = orpy.databases.inversekinematics.InverseKinematicsModel(lrobot, iktype=ik6d)
if not likmodel.load():
    raise Exception("Cannot load ikmodel for left-robot")
rikmodel = orpy.databases.inversekinematics.InverseKinematicsModel(rrobot, iktype=ik6d)
if not rikmodel.load():
    raise Exception("Cannot load ikmodel for right-robot")

if 0:
    # Manipulated object
    mobj = env.ReadKinBodyXMLFile(modelspath + 'chair.xml')
    env.Add(mobj)

    ## Initializing MyObject
    myObject = myobject.MyObject(mobj)
    myObject.SetRestingSurfaceTransform(Trest)
    import threading
    frame = utils.CreateReferenceFrameKinBody(env)
    thread = threading.Thread(target=utils.AttachFrameToBody, args=[frame, mobj, myObject.Tcom])
    thread.start()

    ##########################################################################################

    import pickle
    from pymanip.planners import bquerysolver as bs
    from pymanip.planningutils.utils import OpenRAVEKinBodyWrapper

    with open('test_issuer-260217.pp', 'r') as f:
        placementConnections = pickle.load(f)

    orwrapper = OpenRAVEKinBodyWrapper(mobj, myObject.Tcom)
    solver = bs.BQuerySolver([lrobot, rrobot], orwrapper, myObject, placementConnections)

    case = 1

    if case == 1:
        qobj_start = [7, -0.24385276593254779, 0.018379709112424314, 0.3830841793413482]
        qobj_goal = [7, 0.25252333230092827, -0.034700021668923671, -0.67354369629484889]
    elif case == 2:
        qobj_start = [7, -0.24385276593254779, 0.018379709112424314, 0.3830841793413482]
        # qobj_goal = [2, 0.21999999999999992, 0.010000000000000104, 1.5865042900628454 + np.pi]
        qobj_goal = [2, 0.21999999999999992, 0.010000000000000104, 1.5865042900628454]


    Tstart = myObject.ComputeTObject(qobj_start)
    Tgoal = myObject.ComputeTObject(qobj_goal)

    bquery = bs.BQuery(Tstart, Tgoal)
    manipTraj = solver.Solve(bquery)

else:
    # Manipulated object
    mobj = env.ReadKinBodyXMLFile(modelspath + 'ikea-stefan.xml')
    env.Add(mobj)
    ## Initializing MyObject
    myObject = myobject.MyObject(mobj)
    myObject.SetRestingSurfaceTransform(Trest)
    import threading
    frame = utils.CreateReferenceFrameKinBody(env)
    thread = threading.Thread(target=utils.AttachFrameToBody, args=[frame, mobj, myObject.Tcom])
    thread.start()
    ##########################################################################################
    import pickle
    from pymanip.planners import bquerysolver as bs
    from pymanip.planningutils.utils import OpenRAVEKinBodyWrapper

    # filename = 'data/certificate_ikea-stefan.pp'
    filename = 'data/certificate_ikea-stefan_newenv.pp'
    with open(filename, 'r') as f:
        placementConnections = pickle.load(f)

    orwrapper = OpenRAVEKinBodyWrapper(mobj, myObject.Tcom)
    solver = bs.BQuerySolver([lrobot, rrobot], orwrapper, myObject, placementConnections)

    qobj_start = [257, 0, 0, 0]
    case = 3
    if case == 1:
        qobj_goal = [28, 0.16222713632775759, -0.0083096340700221615, 0.52131594226306521]
    elif case == 2:
        qobj_goal = [28, 0.15073634536689201, 0.0097355158857016823, -2.5654131525883996]
    elif case == 3:
        qobj_start = [210, -0.036219546320382781, 0.037202693046315988, 1.8038674769996976]
        qobj_goal = [28, 0.15073634536689201, 0.0097355158857016823, -2.5654131525883996]
    Tstart = myObject.ComputeTObject(qobj_start)
    Tgoal = myObject.ComputeTObject(qobj_goal)

    bquery = bs.BQuery(Tstart, Tgoal)
    manipTraj = solver.Solve(bquery)


