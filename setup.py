from setuptools import setup

setup(name='pymanip', packages=['pymanip', 'pymanip.planners', 'pymanip.planningutils'],
      include_package_data=True)
